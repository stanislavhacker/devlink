# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [1.0.6]
- *FIX*: Propagate absolute path into **devlink-core** from **cli** due to _MacOS_ behaviour

## [1.0.5]
 - *FIX*: Properly error path in symlink already exists

## [1.0.4] - 2023/02/02
 - *FIX*: Added backup property into LinkedResult

## [1.0.3] - 2022/12/07
 - *FIX*: Export LinkThrow type

## [1.0.2] - 2022/12/07
**FIRST MAJOR RELEASE**

[Unreleased]: https://gitlab.com/stanislavhacker/devlink/-/compare/1.0.6...master
[1.0.6]: https://gitlab.com/stanislavhacker/devlink/-/compare/1.0.5...1.0.6
[1.0.5]: https://gitlab.com/stanislavhacker/devlink/-/compare/1.0.4...1.0.5
[1.0.4]: https://gitlab.com/stanislavhacker/devlink/-/compare/1.0.3...1.0.4
[1.0.3]: https://gitlab.com/stanislavhacker/devlink/-/compare/1.0.2...1.0.3
[1.0.2]: https://gitlab.com/stanislavhacker/devlink/-/compare/init...1.0.2