# ![Servant][logo] devlink-cli

[![Pipeline][pipeline]][link-pipeline]
[![Npm][npm-version]][link-npm]
[![Downloads][npm-downloads]][link-npm]
[![License][license]][link-license]
[![Node][node]][link-node]
[![Collaborators][collaborators]][link-npm]

## What is it?

Devlink is a simple tool, that can create symbolic link in your system from source
to destination file. Usage is really simple and can be used from `command line with arguments`, with `.env` file or `config` file. Devlink keeps original file, so you can always
revert link operation by unlink operation, that's return previous file.

![Devlink preview][preview]

### Possible usages

1. **Simple as a link creator** - Link with ability to remove link
2. **Instead of `yarn` or `npm` link feature** - Yarn or npm link remove reference to previous installed
   package and create new one. And sometimes you need only link dist files or one particular file without
   removing whole package. Devlink can do it.
3. **Linking files from `node_modules`** - Sometimes you need to link file from `node_modules` to
   another place in your project. Devlink can do it.

### Installation and start!

#### 1. Globally installed

You need the latest version of nodejs and then run:

> `npm install devlink-cli -g`

Then you can run in any directory command for creating link, for example

> `devlink link /path/to/original.txt /path/to/linkedFile.txt`

#### 2. Locally installed

You need the latest version of nodejs and then run:

> `npm install devlink-cli`

Then you can run in current directory:

> `"node_modules/.bin/devlink" link /path/to/original.txt /path/to/linkedFile.txt`

or add devlink into `package.json` scripts and run it directly:

```json
{
	"scripts": {
		"make-link": "devlink link /path/to/original.txt /path/to/linkedFile.txt"
	}
}
```

#### 3. Installed with `npx`

You need the latest version of nodejs and then run

> `npx devlink-cli link /path/to/original.txt /path/to/linkedFile.txt`

## Command line API

Devlink has really simple command line api. You can use two modes. `link` and `unlink`.
Link mode create link from source to destination files. Unlink mode remove links and return
original files.

**Creating link or links**

```cmd
devlink link /path/to/original.txt /path/to/linkedFile.txt
```

or more links

```cmd
devlink link /path/to/original.txt /path/to/linkedFile.txt /path/to/original1.txt /path/to/linkedFile1.txt
```

**Removing link or links**

```cmd
devlink unlink /path/to/original.txt
```

or more links

```cmd
devlink unlink /path/to/original.txt /path/to/original1.txt
```

## Using config `.link.json`

Devlink can use also config file `.link.json` in current directory. Config file is simple json file
with array of links. For link mode, every first link is source and every second is destination. Source is
path to original file and destination is path to linked file. For unlink mode, every link is path to
original file. You can also specify `mode` in config file, but that's not recommend because is more suitable
to specify mode in command line.

**`.link.json`**

```json
{
	"mode": "link", //not recommend, use mode in commnad line
	"links": [
		"/path/to/original.txt",
		"/path/to/linkedFile.txt",
		"/path/to/original1.txt",
		"/path/to/linkedFile1.txt"
	],
	"unlinks": ["/path/to/original.txt", "/path/to/original1.txt"]
}
```

## Using `.env` file

Devlink can use also environmental file `.env` in current directory. This file is simple key value file
with links split by lines or comma. For link mode, every first link is source and every second is destination. Source is
path to original file and destination is path to linked file. For unlink mode, every link is path to
original file. You can also specify `mode` in environmental file, but that's not recommend because is more suitable
to specify mode in command line.

**`.env`**

```env
mode=link #not recommend, use mode in commnad line
links=/path/to/original.txt,/path/to/linkedFile.txt
links=/path/to/original1.txt,/path/to/linkedFile1.txt
unlinks=/path/to/original.txt
unlinks=/path/to/original1.txt
```

## Node API

Devlink can be used also in nodejs. You can use `link` and `unlink` functions. Link function create link
from source to destination files. Unlink function remove links and return original files. There are more
ways how to use this api. It's implemented as **functional**, **promise**, **throwable** and with **callbacks**.
For more information about api, see [API documentation][link-api] in [`devlink` package][link-devlink-package].

### Donate me

| QR                                                                                                           | Paypal                                                                                                                                                              |
| ------------------------------------------------------------------------------------------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| ![](https://gitlab.com/uploads/-/system/personal_snippet/1929487/66399a49a06fa8eb9a0758b8673758c5/qr_sh.png) | [![](https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=DUT8W343NVGQQ&source=url) |

[logo]: https://gitlab.com/stanislavhacker/devlink/raw/master/logo.png
[preview]: https://gitlab.com/stanislavhacker/devlink/raw/master/assets/preview.jpg
[pipeline]: https://gitlab.com/stanislavhacker/devlink/badges/master/pipeline.svg
[npm-version]: https://img.shields.io/npm/v/devlink-cli.svg
[npm-downloads]: https://img.shields.io/npm/dm/devlink-cli.svg
[license]: https://img.shields.io/npm/l/devlink-cli.svg
[node]: https://img.shields.io/node/v/devlink-cli.svg
[collaborators]: https://img.shields.io/npm/collaborators/devlink-cli.svg
[issues]: https://gitlab.com/stanislavhacker/devlink/-/issues/
[link-license]: https://gitlab.com/stanislavhacker/devlink/blob/master/LICENSE
[link-npm]: https://www.npmjs.com/package/devlink-cli
[link-pipeline]: https://gitlab.com/stanislavhacker/devlink/pipelines
[link-node]: https://nodejs.org/en/
[link-api]: https://gitlab.com/stanislavhacker/devlink/-/blob/master/module.devlink/README.md
[link-devlink-package]: https://www.npmjs.com/package/devlink-core
