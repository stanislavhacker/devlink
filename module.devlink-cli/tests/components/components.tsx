/* eslint-disable @typescript-eslint/no-non-null-assertion */
import * as React from "react";
import "jasmine";
import { render } from "ink-testing-library";
import * as O from "fp-ts/Option";
import * as E from "fp-ts/Either";

import {
	error,
	errorOption,
	errorFromSome,
	errorFromNone,
	errorsFromOptions,
	Error as ErrorComponent,
	Main,
} from "../../src/components";
import { ErrorCode, ERRORS } from "../../src/model";

//internal
import { Results } from "../../src/components/devlink/Result";
import { Error as ErrorDevlinkComponent } from "../../src/components/devlink/Error";
import { Unlink, Link } from "../../src/components/devlink/Actions";
import { Fail } from "../../src/components/devlink/Fail";
import { Errors } from "../../src/components/Errors";

import { clearOutput, eitherToError, eitherToValue, optionAsValue } from "../utils";

describe("components", () => {
	describe("Error component", () => {
		it("error simple only", () => {
			const data = error({
				title: "Title",
				description: "Error description",
				code: ErrorCode.MISSING_FILES,
			});
			expect(data.exitCode).toBe(2);
			expect(data.code).toBe(ErrorCode.MISSING_FILES);

			const { lastFrame } = render(data.component);
			expect(clearOutput(lastFrame())).toEqual([
				" devlink  [ERROR] Title: Error description as MISSING_FILES",
			]);
		});

		it("error simple only with tip", () => {
			const data = error({
				title: "Title",
				description: "Error description",
				code: ErrorCode.MISSING_FILES,
				tip: "Add files into command",
			});
			expect(data.exitCode).toBe(2);
			expect(data.code).toBe(ErrorCode.MISSING_FILES);

			const { lastFrame } = render(data.component);
			expect(clearOutput(lastFrame())).toEqual([
				" devlink  [ERROR] Title: Error description as MISSING_FILES",
				"          TIP: Add files into command",
			]);
		});

		it("error simple only with tip and error", () => {
			const data = error(
				{
					title: "Title",
					description: "Error description",
					code: ErrorCode.MISSING_FILES,
					tip: "Add files into command",
				},
				new Error("Original")
			);
			expect(data.exitCode).toBe(2);
			expect(data.code).toBe(ErrorCode.MISSING_FILES);

			const { lastFrame } = render(data.component);
			const screen = clearOutput(lastFrame());
			expect(screen.slice(0, 5)).toEqual([
				" devlink  [ERROR] Title: Error description as MISSING_FILES",
				"          TIP: Add files into command",
				"",
				"  ─────── Original error ───────",
				" Error: Original",
			]);
			expect(screen.length).toBeGreaterThan(6);
		});

		it("Error component only", () => {
			const { lastFrame } = render(
				<ErrorComponent title="Test" description="Desc" id="id" tip="Tip" />
			);
			const screen = clearOutput(lastFrame());
			expect(screen.slice(0, 5)).toEqual([
				" devlink  [ERROR] Test: Desc as id",
				"          TIP: Tip",
			]);
		});

		it("error option that is none", () => {
			const data = errorOption(O.none, {
				title: "Title",
				description: "Error description",
				code: ErrorCode.MISSING_FILES,
				tip: "Add files into command",
			});

			expect(E.isRight(data)).toBe(false);
			expect(E.isLeft(data)).toBe(true);

			const err = eitherToError(data)!;
			expect(err.code).toBe(ErrorCode.MISSING_FILES);
		});

		it("error option that is defined", () => {
			const data = errorOption(O.some("name"), {
				title: "Title",
				description: "Error description",
				code: ErrorCode.MISSING_FILES,
				tip: "Add files into command",
			});

			expect(E.isRight(data)).toBe(true);
			expect(E.isLeft(data)).toBe(false);

			const value = eitherToValue(data)!;
			expect(value).toBe("name");
		});

		it("error from none Option none", () => {
			const err = {
				title: "Title",
				description: "Error description",
				code: ErrorCode.MISSING_FILES,
				tip: "Add files into command",
			};
			const data = errorFromNone(O.none, err);

			const value = optionAsValue(data)!;
			expect(value).toEqual(err);
		});

		it("error from none Option some", () => {
			const err = {
				title: "Title",
				description: "Error description",
				code: ErrorCode.MISSING_FILES,
				tip: "Add files into command",
			};
			const data = errorFromNone(O.some(true), err);

			const value = optionAsValue(data);
			expect(value).toBe(null);
		});

		it("error from some Option some", () => {
			const err = {
				title: "Title",
				description: "Error description",
				code: ErrorCode.MISSING_FILES,
				tip: "Add files into command",
			};
			const data = errorFromSome(O.some(true), err);

			const value = optionAsValue(data)!;
			expect(value).toEqual(err);
		});

		it("error from some Option none", () => {
			const err = {
				title: "Title",
				description: "Error description",
				code: ErrorCode.MISSING_FILES,
				tip: "Add files into command",
			};
			const data = errorFromSome(O.none, err);

			const value = optionAsValue(data);
			expect(value).toBe(null);
		});

		it("errors from options array all some", () => {
			const err = {
				title: "Title",
				description: "Error description",
				code: ErrorCode.MISSING_FILES,
				tip: "Add files into command",
			};
			const errors = errorsFromOptions([O.some(err), O.some(err), O.some(err)]);

			expect(errors.length).toBe(3);
			expect(errors).toEqual([err, err, err]);
		});

		it("errors from options array one some", () => {
			const err = {
				title: "Title",
				description: "Error description",
				code: ErrorCode.MISSING_FILES,
				tip: "Add files into command",
			};
			const errors = errorsFromOptions([O.none, O.some(err), O.none]);

			expect(errors.length).toBe(1);
			expect(errors).toEqual([err]);
		});
	});

	describe("Errors component", () => {
		it("no errors", () => {
			const { lastFrame } = render(<Errors mode="link" errors={[]} />);

			const screen = clearOutput(lastFrame());
			expect(screen).toEqual([""]);
		});

		it("one error in link mode", () => {
			const { lastFrame } = render(
				<Errors
					mode="link"
					errors={[
						{
							on: "/path/to/source.txt",
							to: "/path/to/link.txt",
							errors: [ERRORS.MISSING_FILES],
						},
					]}
				/>
			);

			const screen = clearOutput(lastFrame());
			expect(screen.slice(0, 5)).toEqual([
				"",
				"Invalid files",
				" devlink  Operation failed Linking  +sl /path/to/source.txt => /path/to/link.txt",
				" devlink  [ERROR] No paths to files provided: You need specified file or directory paths that will",
				'                  be used for "link" or "unlink" as MISSING_FILES',
			]);
		});

		it("one error in unlink mode", () => {
			const { lastFrame } = render(
				<Errors
					mode="unlink"
					errors={[
						{
							on: "/path/to/source.txt",
							to: "/path/to/source.txt",
							errors: [ERRORS.MISSING_FILES],
						},
					]}
				/>
			);

			const screen = clearOutput(lastFrame());
			expect(screen.slice(0, 5)).toEqual([
				"",
				"Invalid files",
				" devlink  Operation failed Unlinking  -sl /path/to/source.txt",
				" devlink  [ERROR] No paths to files provided: You need specified file or directory paths that will",
				'                  be used for "link" or "unlink" as MISSING_FILES',
			]);
		});

		it("more errors in link mode", () => {
			const { lastFrame } = render(
				<Errors
					mode="link"
					errors={[
						{
							on: "/path/to/source.txt",
							to: "/path/to/link.txt",
							errors: [ERRORS.MISSING_FILES],
						},
						{
							on: "/path/to/source1.txt",
							to: "/path/to/link1.txt",
							errors: [ERRORS.MISSING_FILES],
						},
					]}
				/>
			);

			const screen = clearOutput(lastFrame());
			expect(screen).toEqual([
				"",
				"Invalid files",
				" devlink  Operation failed Linking  +sl /path/to/source.txt => /path/to/link.txt",
				" devlink  [ERROR] No paths to files provided: You need specified file or directory paths that will",
				'                  be used for "link" or "unlink" as MISSING_FILES',
				'          TIP: Use command line with defined files for example: "devlink link /path/to/source.txt',
				'               /path/to/destination.txt"',
				" devlink  Operation failed Linking  +sl /path/to/source1.txt => /path/to/link1.txt",
				" devlink  [ERROR] No paths to files provided: You need specified file or directory paths that will",
				'                  be used for "link" or "unlink" as MISSING_FILES',
				'          TIP: Use command line with defined files for example: "devlink link /path/to/source.txt',
				'               /path/to/destination.txt"',
			]);
		});

		it("one error in link mode with more inside errors", () => {
			const { lastFrame } = render(
				<Errors
					mode="link"
					errors={[
						{
							on: "/path/to/source.txt",
							to: "/path/to/link.txt",
							errors: [ERRORS.MISSING_FILES, ERRORS.FILE_NOT_EXISTS],
						},
					]}
				/>
			);

			const screen = clearOutput(lastFrame());
			expect(screen.slice(0, 5)).toEqual([
				"",
				"Invalid files",
				" devlink  Operation failed Linking  +sl /path/to/source.txt => /path/to/link.txt",
				" devlink  [ERROR] File not exist on path: Desired file not exists on provided file path as",
				"                  FILE_NOT_EXISTS",
			]);
		});
	});

	describe("Main component", () => {
		it("render with config, env and results ok", () => {
			const { lastFrame } = render(
				<Main
					mode="link"
					env={O.some("./.env")}
					config={O.some("./.link.json")}
					errors={[]}
					results={[
						{
							result: E.right({
								on: "/path/to/source.txt",
								to: "/path/to/link.txt",
								original: "/path/to/##orig_source.txt",
							}),
							request: {
								on: "/path/to/source.txt",
								to: "/path/to/link.txt",
								errors: [],
							},
						},
					]}
				/>
			);

			const screen = clearOutput(lastFrame());
			expect(screen).toEqual([
				" devlink  link",
				" devlink  [config]       ./.link.json",
				" devlink  [environment]  ./.env",
				" devlink   +sl /path/to/source.txt => /path/to/link.txt",
			]);
		});

		it("render with config, env and results error", () => {
			const { lastFrame } = render(
				<Main
					mode="link"
					env={O.some("./.env")}
					config={O.some("./.link.json")}
					errors={[]}
					results={[
						{
							result: E.left({
								name: "Title",
								description: "Desc",
								type: "SYMLINK_EXISTS_ALREADY",
							}),
							request: {
								on: "/path/to/source.txt",
								to: "/path/to/link.txt",
								errors: [],
							},
						},
					]}
				/>
			);

			const screen = clearOutput(lastFrame());
			expect(screen).toEqual([
				" devlink  link",
				" devlink  [config]       ./.link.json",
				" devlink  [environment]  ./.env",
				" devlink  Operation failed Linking  +sl /path/to/source.txt => /path/to/link.txt",
				" devlink  [ERROR] Title: Desc as SYMLINK_EXISTS_ALREADY",
			]);
		});

		it("render errors only with 1 error", () => {
			const { lastFrame } = render(
				<Main
					mode="link"
					env={O.none}
					config={O.none}
					errors={[
						{
							on: "/path/to/source.txt",
							to: "/path/to/link.txt",
							errors: [ERRORS.FILES_INVALID_TYPE],
						},
					]}
					results={[]}
				/>
			);

			const screen = clearOutput(lastFrame());
			expect(screen).toEqual([
				" devlink  link",
				"",
				"Invalid files",
				" devlink  Operation failed Linking  +sl /path/to/source.txt => /path/to/link.txt",
				" devlink  [ERROR] Files are unknown or has different types: Provided files into current mode has",
				"                  different types, or type of one is unknown as FILES_INVALID_TYPE",
			]);
		});
	});

	describe("devlink action Fail component", () => {
		it("Fail component for link", () => {
			const { lastFrame } = render(
				<Fail
					mode="link"
					on="/path/to/source.txt"
					to="/path/to/link.txt"
					title="Title"
					description="Description"
					id="ID"
					tip="Tip"
				/>
			);

			const screen = clearOutput(lastFrame());
			expect(screen).toEqual([
				" devlink  Operation failed Linking  +sl /path/to/source.txt => /path/to/link.txt",
				" devlink  [ERROR] Title: Description as ID",
				"          TIP: Tip",
			]);
		});

		it("Fail component for unlink", () => {
			const { lastFrame } = render(
				<Fail
					mode="unlink"
					on="/path/to/source.txt"
					to="/path/to/link.txt"
					title="Title"
					description="Description"
					id="ID"
					tip="Tip"
				/>
			);

			const screen = clearOutput(lastFrame());
			expect(screen).toEqual([
				" devlink  Operation failed Unlinking  -sl /path/to/source.txt",
				" devlink  [ERROR] Title: Description as ID",
				"          TIP: Tip",
			]);
		});
	});

	describe("devlink action Link component", () => {
		it("Link component", () => {
			const { lastFrame } = render(<Link on="/path/to/source.txt" to="/path/to/link.txt" />);

			const screen = clearOutput(lastFrame());
			expect(screen).toEqual([" +sl /path/to/source.txt => /path/to/link.txt"]);
		});
	});

	describe("devlink action Unlink component", () => {
		it("Unlink component", () => {
			const { lastFrame } = render(<Unlink on="/path/to/source.txt" />);

			const screen = clearOutput(lastFrame());
			expect(screen).toEqual([" -sl /path/to/source.txt"]);
		});
	});

	describe("devlink Error component", () => {
		it("render error", () => {
			const { lastFrame } = render(
				<ErrorDevlinkComponent
					mode="link"
					error={{
						name: "Err",
						description: "Desc",
						type: "RENAME_FILE_ERROR",
						original: new Error("Test"),
					}}
					request={{ on: "/path/to/source.txt", to: "/path/to/link.txt", errors: [] }}
				/>
			);

			const screen = clearOutput(lastFrame());
			expect(screen.slice(0, 5)).toEqual([
				" devlink  Operation failed Linking  +sl /path/to/source.txt => /path/to/link.txt",
				" devlink  [ERROR] Err: Desc as RENAME_FILE_ERROR",
				"",
				"  ─────── Original error ───────",
				" Error: Test",
			]);
			expect(screen.length).toBeGreaterThan(5);
		});
	});

	describe("devlink Result component", () => {
		it("render link result", () => {
			const { lastFrame } = render(
				<Results
					mode="link"
					result={{
						on: "/path/to/source.txt",
						to: "/path/to/link.txt",
						original: "/path/to/##orig_source.txt",
					}}
					request={{ on: "/path/to/source.txt", to: "/path/to/link.txt", errors: [] }}
				/>
			);

			const screen = clearOutput(lastFrame());
			expect(screen).toEqual([" devlink   +sl /path/to/source.txt => /path/to/link.txt"]);
		});

		it("render unlink result", () => {
			const { lastFrame } = render(
				<Results
					mode="unlink"
					result={{ on: "/path/to/source.txt" }}
					request={{ on: "/path/to/source.txt", to: "/path/to/source.txt", errors: [] }}
				/>
			);

			const screen = clearOutput(lastFrame());
			expect(screen).toEqual([" devlink   -sl /path/to/source.txt"]);
		});
	});
});
