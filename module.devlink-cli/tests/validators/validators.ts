/* eslint-disable @typescript-eslint/no-non-null-assertion */
import * as O from "fp-ts/Option";
import * as E from "fp-ts/Either";
import { functional } from "devlink-core";

import { filledMode, linkMode, unlinkMode } from "../../src/validators/mode";
import { filledFiles, linkFiles, unlinkFiles } from "../../src/validators/files";
import { validateInputs, validateMode, validateFiles } from "../../src/validators";
import { ErrorCode, ERRORS } from "../../src/model";

import { eitherToValue, eitherToError } from "../utils";
import { createStore } from "../store";

describe("validators", () => {
	describe("filledMode", () => {
		it("none", () => {
			const test = filledMode(O.none)({});
			expect(E.isRight(test)).toBe(false);
			expect(E.isLeft(test)).toBe(true);

			const err = eitherToError(test)!;
			expect(err.code).toBe(ErrorCode.MISSING_MODE);
			expect(err.exitCode).toBe(1);
			expect(err.component).toBeDefined();
		});

		it("some", () => {
			const test = filledMode(O.some("link"))({});
			expect(E.isRight(test)).toBe(true);
			expect(E.isLeft(test)).toBe(false);

			const { mode } = eitherToValue(test)!;
			expect(mode).toBe("link");
		});
	});

	describe("linkMode", () => {
		it("link but no files ", () => {
			const test = linkMode({ cwd: "/", mode: "link", files: [] });
			expect(E.isRight(test)).toBe(false);
			expect(E.isLeft(test)).toBe(true);

			const err = eitherToError(test)!;
			expect(err.code).toBe(ErrorCode.INVALID_LINK_FILES_COUNT);
			expect(err.exitCode).toBe(3);
			expect(err.component).toBeDefined();
		});

		it("link but invalid count of files", () => {
			const test = linkMode({ cwd: "/", mode: "link", files: ["/path/to/source.txt"] });
			expect(E.isRight(test)).toBe(false);
			expect(E.isLeft(test)).toBe(true);

			const err = eitherToError(test)!;
			expect(err.code).toBe(ErrorCode.INVALID_LINK_FILES_COUNT);
			expect(err.exitCode).toBe(3);
			expect(err.component).toBeDefined();
		});

		it("link but invalid count of files", () => {
			const test = linkMode({
				cwd: "/",
				mode: "link",
				files: ["/path/to/source.txt", "/path/to/link.txt", "/path/to/source1.txt"],
			});
			expect(E.isRight(test)).toBe(false);
			expect(E.isLeft(test)).toBe(true);

			const err = eitherToError(test)!;
			expect(err.code).toBe(ErrorCode.INVALID_LINK_FILES_COUNT);
			expect(err.exitCode).toBe(3);
			expect(err.component).toBeDefined();
		});

		it("link valid", () => {
			const test = linkMode({
				cwd: "/",
				mode: "link",
				files: ["/path/to/source.txt", "/path/to/link.txt"],
			});
			expect(E.isRight(test)).toBe(true);
			expect(E.isLeft(test)).toBe(false);

			const value = eitherToValue(test)!;
			expect(value).toEqual({
				cwd: "/",
				mode: "link",
				files: ["/path/to/source.txt", "/path/to/link.txt"],
			});
		});
	});

	describe("unlinkMode", () => {
		it("unlink but no files ", () => {
			const test = unlinkMode({ cwd: "/", mode: "unlink", files: [] });
			expect(E.isRight(test)).toBe(false);
			expect(E.isLeft(test)).toBe(true);

			const err = eitherToError(test)!;
			expect(err.code).toBe(ErrorCode.INVALID_UNLINK_FILES_COUNT);
			expect(err.exitCode).toBe(3);
			expect(err.component).toBeDefined();
		});

		it("unlink valid", () => {
			const test = unlinkMode({ cwd: "/", mode: "unlink", files: ["/path/to/source.txt"] });
			expect(E.isRight(test)).toBe(true);
			expect(E.isLeft(test)).toBe(false);

			const value = eitherToValue(test)!;
			expect(value).toEqual({ cwd: "/", mode: "unlink", files: ["/path/to/source.txt"] });
		});
	});

	describe("filledFiles", () => {
		it("none", () => {
			const test = filledFiles(O.none)({});
			expect(E.isRight(test)).toBe(false);
			expect(E.isLeft(test)).toBe(true);

			const err = eitherToError(test)!;
			expect(err.code).toBe(ErrorCode.MISSING_FILES);
			expect(err.exitCode).toBe(2);
			expect(err.component).toBeDefined();
		});

		it("some", () => {
			const test = filledFiles(O.some(["file.txt"]))({});
			expect(E.isRight(test)).toBe(true);
			expect(E.isLeft(test)).toBe(false);

			const { files } = eitherToValue(test)!;
			expect(files).toEqual(["file.txt"]);
		});
	});

	describe("linkFiles", () => {
		let clean;

		beforeEach(() => {
			clean = createStore();
		});

		it("valid files check", () => {
			const result = linkFiles({
				cwd: "/",
				mode: "link",
				files: ["/data/sources/source.txt", "/data/links/link.txt"],
			});
			expect(E.isRight(result)).toBe(true);
			expect(E.isLeft(result)).toBe(false);

			const value = eitherToValue(result)!;
			expect(value).toEqual({
				cwd: "/",
				mode: "link",
				files: [{ on: "/data/sources/source.txt", to: "/data/links/link.txt", errors: [] }],
			});
		});

		it("not existing source check", () => {
			const result = linkFiles({
				cwd: "/",
				mode: "link",
				files: ["/data/sources/source1.txt", "/data/links/link.txt"],
			});
			expect(E.isRight(result)).toBe(true);
			expect(E.isLeft(result)).toBe(false);

			const value = eitherToValue(result)!;
			expect(value).toEqual({
				cwd: "/",
				mode: "link",
				files: [
					{
						on: "/data/sources/source1.txt",
						to: "/data/links/link.txt",
						errors: [ERRORS.FILE_NOT_EXISTS, ERRORS.FILES_INVALID_TYPE],
					},
				],
			});
		});

		it("already a link source check", () => {
			functional.link("/data/sources/source.txt", "/data/links/link.txt");

			const result = linkFiles({
				cwd: "/",
				mode: "link",
				files: ["/data/sources/source.txt", "/data/links/link.txt"],
			});
			expect(E.isRight(result)).toBe(true);
			expect(E.isLeft(result)).toBe(false);

			const value = eitherToValue(result)!;
			expect(value).toEqual({
				cwd: "/",
				mode: "link",
				files: [
					{
						on: "/data/sources/source.txt",
						to: "/data/links/link.txt",
						errors: [ERRORS.FILE_IS_SYMLINK_ALREADY, ERRORS.FILES_INVALID_TYPE],
					},
				],
			});
		});

		afterEach(() => {
			clean();
		});
	});

	describe("unlinkFiles", () => {
		let clean;

		beforeEach(() => {
			clean = createStore();
		});

		it("valid files check", () => {
			functional.link("/data/sources/source.txt", "/data/links/link.txt");

			const result = unlinkFiles({
				cwd: "/",
				mode: "unlink",
				files: ["/data/sources/source.txt"],
			});
			expect(E.isRight(result)).toBe(true);
			expect(E.isLeft(result)).toBe(false);

			const value = eitherToValue(result)!;
			expect(value).toEqual({
				cwd: "/",
				mode: "unlink",
				files: [
					{ on: "/data/sources/source.txt", to: "/data/sources/source.txt", errors: [] },
				],
			});
		});

		it("not existing source check", () => {
			const result = unlinkFiles({
				cwd: "/",
				mode: "unlink",
				files: ["/data/sources/source1.txt"],
			});
			expect(E.isRight(result)).toBe(true);
			expect(E.isLeft(result)).toBe(false);

			const value = eitherToValue(result)!;
			expect(value).toEqual({
				cwd: "/",
				mode: "unlink",
				files: [
					{
						on: "/data/sources/source1.txt",
						to: "/data/sources/source1.txt",
						errors: [
							ERRORS.FILE_NOT_EXISTS,
							ERRORS.FILE_IS_NOT_SYMLINK,
							ERRORS.FILES_INVALID_TYPE,
						],
					},
				],
			});
		});

		it("not a link source check", () => {
			const result = unlinkFiles({
				cwd: "/",
				mode: "unlink",
				files: ["/data/sources/source.txt"],
			});
			expect(E.isRight(result)).toBe(true);
			expect(E.isLeft(result)).toBe(false);

			const value = eitherToValue(result)!;
			expect(value).toEqual({
				cwd: "/",
				mode: "unlink",
				files: [
					{
						on: "/data/sources/source.txt",
						to: "/data/sources/source.txt",
						errors: [ERRORS.FILE_IS_NOT_SYMLINK],
					},
				],
			});
		});

		afterEach(() => {
			clean();
		});
	});

	//index

	describe("validateInputs", () => {
		it("not filled", () => {
			const test = validateInputs({ cwd: "/", mode: O.none, files: O.none });

			expect(E.isRight(test)).toBe(false);
			expect(E.isLeft(test)).toBe(true);

			const err = eitherToError(test)!;
			expect(err.code).toBe(ErrorCode.MISSING_MODE);
			expect(err.exitCode).toBe(1);
			expect(err.component).toBeDefined();
		});

		it("filled", () => {
			const test = validateInputs({
				cwd: "/",
				mode: O.some("link"),
				files: O.some(["/path/to/source.txt"]),
			});

			expect(E.isRight(test)).toBe(true);
			expect(E.isLeft(test)).toBe(false);

			const { mode, files } = eitherToValue(test)!;
			expect(mode).toBe("link");
			expect(files).toEqual(["/path/to/source.txt"]);
		});
	});

	describe("validateMode", () => {
		it("not filled", () => {
			const test = validateMode({ cwd: "/", mode: "unlink", files: [] });

			expect(E.isRight(test)).toBe(false);
			expect(E.isLeft(test)).toBe(true);
		});

		it("filled", () => {
			const test = validateMode({ cwd: "/", mode: "unlink", files: ["/path/to/source.txt"] });

			expect(E.isRight(test)).toBe(true);
			expect(E.isLeft(test)).toBe(false);
		});
	});

	describe("validateFiles", () => {
		let clean;

		beforeEach(() => {
			clean = createStore();
		});

		it("valid files check for link", () => {
			const result = validateFiles({
				cwd: "/",
				mode: "link",
				files: ["/data/sources/source.txt", "/data/links/link.txt"],
			});
			expect(E.isRight(result)).toBe(true);
			expect(E.isLeft(result)).toBe(false);

			const value = eitherToValue(result)!;
			expect(value).toEqual({
				cwd: "/",
				mode: "link",
				files: [{ on: "/data/sources/source.txt", to: "/data/links/link.txt", errors: [] }],
			});
		});

		it("invalid files check for link", () => {
			const result = validateFiles({
				cwd: "/",
				mode: "link",
				files: ["/data/sources/source1.txt", "/data/sources/link1.txt"],
			});
			expect(E.isRight(result)).toBe(true);
			expect(E.isLeft(result)).toBe(false);

			const value = eitherToValue(result)!;
			expect(value).toEqual({
				cwd: "/",
				mode: "link",
				files: [
					{
						on: "/data/sources/source1.txt",
						to: "/data/sources/link1.txt",
						errors: [ERRORS.FILE_NOT_EXISTS, ERRORS.FILES_INVALID_TYPE],
					},
				],
			});
		});

		afterEach(() => {
			clean();
		});
	});
});
