/* eslint-disable @typescript-eslint/no-non-null-assertion */
import "jasmine";
import { render } from "ink-testing-library";
import * as O from "fp-ts/Option";

import { app } from "../../src/app";
import { createStore } from "../store";
import { clearOutput } from "../utils";

describe("app", () => {
	let clean;

	beforeEach(() => {
		clean = createStore();
	});

	it("without any parameters", () => {
		const { lastFrame } = render(
			app({ cwd: "/", mode: O.none, files: O.none, env: O.none, config: O.none })
		);
		expect(clearOutput(lastFrame())).toEqual([
			" devlink  [ERROR] No mode provided to devlink: Devlink needs to known if you want to create or",
			'                  remove link. Possible values are "link" or "unlink" as MISSING_MODE',
			'          TIP: Use command line with defined mode for example: "devlink link /path/to/source.txt',
			'               /path/to/destination.txt"',
		]);
	});

	it("with valid parameters", () => {
		const { lastFrame } = render(
			app({
				cwd: "/",
				mode: O.some("link"),
				files: O.some(["/data/sources/source.txt", "/data/links/link.txt"]),
				env: O.some("/path/to/.env"),
				config: O.some("/path/to/.link.json"),
			})
		);
		expect(clearOutput(lastFrame())).toEqual([
			" devlink  link",
			" devlink  [config]       /path/to/.link.json",
			" devlink  [environment]  /path/to/.env",
			" devlink   +sl /data/sources/source.txt => /data/links/link.txt",
		]);
	});

	afterEach(() => {
		clean();
	});
});
