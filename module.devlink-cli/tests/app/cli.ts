/* eslint-disable @typescript-eslint/no-non-null-assertion */
import "jasmine";
import * as O from "fp-ts/Option";
import * as path from "path";

import { parse, usedConfig, get } from "../../src/cli";
import { optionAsValue } from "../utils";

describe("cli", () => {
	describe("parse", () => {
		it("cmd empty array", () => {
			const { mode, files } = parse({}, []);
			expect(O.isNone(mode)).toBeTrue();
			expect(O.isNone(files)).toBeTrue();
		});

		it("cmd random data", () => {
			const { mode, files } = parse({}, ["test", "aaa", "muhehe"]);
			expect(O.isNone(mode)).toBeTrue();
			expect(O.isNone(files)).toBeTrue();
		});

		it("cmd link mode, no files", () => {
			const { mode, files } = parse({}, ["link"]);
			expect(O.isSome(mode)).toBeTrue();
			expect(optionAsValue(mode)).toBe("link");
			expect(O.isNone(files)).toBeTrue();
		});

		it("cmd unlink mode, no files", () => {
			const { mode, files } = parse({}, ["unlink"]);
			expect(O.isSome(mode)).toBeTrue();
			expect(optionAsValue(mode)).toBe("unlink");
			expect(O.isNone(files)).toBeTrue();
		});

		it("cmd link mode and files", () => {
			const { mode, files } = parse({}, ["link", "/path/to/file.txt", "/path/to/source.txt"]);
			expect(O.isSome(mode)).toBeTrue();
			expect(optionAsValue(mode)).toBe("link");
			expect(O.isSome(files)).toBeTrue();
			expect(optionAsValue(files)).toEqual(["/path/to/file.txt", "/path/to/source.txt"]);
		});

		it("config link mode, no files", () => {
			const { mode, files } = parse({ mode: "link" }, []);
			expect(O.isSome(mode)).toBeTrue();
			expect(optionAsValue(mode)).toBe("link");
			expect(O.isNone(files)).toBeTrue();
		});

		it("config link mode and links files", () => {
			const { mode, files } = parse(
				{ mode: "link", links: ["/path/to/file.txt", "/path/to/source.txt"] },
				[]
			);
			expect(O.isSome(mode)).toBeTrue();
			expect(optionAsValue(mode)).toBe("link");
			expect(O.isSome(files)).toBeTrue();
			expect(optionAsValue(files)).toEqual(["/path/to/file.txt", "/path/to/source.txt"]);
		});

		it("config link mode and links file", () => {
			const { mode, files } = parse({ mode: "link", links: "/path/to/file.txt" }, []);
			expect(O.isSome(mode)).toBeTrue();
			expect(optionAsValue(mode)).toBe("link");
			expect(O.isSome(files)).toBeTrue();
			expect(optionAsValue(files)).toEqual(["/path/to/file.txt"]);
		});

		it("config link mode and links files, override by cmd mode", () => {
			const { mode, files } = parse(
				{ mode: "link", links: ["/path/to/file.txt", "/path/to/source.txt"] },
				["unlink"]
			);
			expect(O.isSome(mode)).toBeTrue();
			expect(optionAsValue(mode)).toBe("unlink");
			expect(O.isNone(files)).toBeTrue();
		});

		it("config link mode and links files, override by cmd mode and files", () => {
			const { mode, files } = parse(
				{ mode: "link", links: ["/path/to/file.txt", "/path/to/source.txt"] },
				["unlink", "/path/to/source1.txt"]
			);
			expect(O.isSome(mode)).toBeTrue();
			expect(optionAsValue(mode)).toBe("unlink");
			expect(O.isSome(files)).toBeTrue();
			expect(optionAsValue(files)).toEqual(["/path/to/source1.txt"]);
		});

		it("config unlink and links files", () => {
			const { mode, files } = parse(
				{ mode: "unlink", links: ["/path/to/file.txt", "/path/to/source.txt"] },
				[]
			);
			expect(O.isSome(mode)).toBeTrue();
			expect(optionAsValue(mode)).toBe("unlink");
			expect(O.isNone(files)).toBeTrue();
		});

		it("config unlink and unlinks file", () => {
			const { mode, files } = parse({ mode: "unlink", unlinks: ["/path/to/file.txt"] }, []);
			expect(O.isSome(mode)).toBeTrue();
			expect(optionAsValue(mode)).toBe("unlink");
			expect(O.isSome(files)).toBeTrue();
			expect(optionAsValue(files)).toEqual(["/path/to/file.txt"]);
		});

		it("config unlink file and cmd unlink", () => {
			const { mode, files } = parse({ unlinks: ["/path/to/file.txt"] }, ["unlink"]);
			expect(O.isSome(mode)).toBeTrue();
			expect(optionAsValue(mode)).toBe("unlink");
			expect(O.isSome(files)).toBeTrue();
			expect(optionAsValue(files)).toEqual(["/path/to/file.txt"]);
		});

		it("config link files and cmd link", () => {
			const { mode, files } = parse({ links: ["/path/to/file.txt", "/path/to/source.txt"] }, [
				"link",
			]);
			expect(O.isSome(mode)).toBeTrue();
			expect(optionAsValue(mode)).toBe("link");
			expect(O.isSome(files)).toBeTrue();
			expect(optionAsValue(files)).toEqual(["/path/to/file.txt", "/path/to/source.txt"]);
		});

		it("config link files grouped and cmd link", () => {
			const { mode, files } = parse({ links: ["/path/to/file.txt,/path/to/source.txt"] }, [
				"link",
			]);
			expect(O.isSome(mode)).toBeTrue();
			expect(optionAsValue(mode)).toBe("link");
			expect(O.isSome(files)).toBeTrue();
			expect(optionAsValue(files)).toEqual(["/path/to/file.txt", "/path/to/source.txt"]);
		});
	});

	describe("usedConfig", () => {
		it("not used", () => {
			const config = usedConfig({ path: "/path/to/config", used: false });
			expect(O.isNone(config)).toBeTrue();
		});

		it("used", () => {
			const config = usedConfig({ path: "/path/to/config", used: true });
			expect(O.isSome(config)).toBeTrue();
			expect(optionAsValue(config)).toEqual("/path/to/config");
		});
	});

	describe("get", () => {
		function createProcess(argv: string, env: NodeJS.ProcessEnv = {}): NodeJS.Process {
			const proc = jasmine.createSpyObj<NodeJS.Process>("process", ["cwd"]);

			proc.cwd.and.returnValue("/project/path/to/dir");
			proc.env = env;
			proc.argv = ["node", "index.js"].concat(argv.length ? argv.split(" ") : []);

			return proc;
		}

		it("empty command line", () => {
			const cli = get(createProcess(""));
			expect(cli.config.path).toContain(path.normalize("/project/path/to/dir/.link.json"));
			expect(cli.config.used).toEqual(false);
			expect(cli.env.path).toContain(path.normalize("/project/path/to/dir/.env"));
			expect(cli.env.used).toEqual(false);
			expect(cli.$).toEqual({
				mode: "",
				links: [],
				unlinks: [],
			});
			expect(cli._).toEqual([]);
			expect(cli["--"]).toEqual([]);
		});

		it("command line with mode and files", () => {
			const cli = get(createProcess("link /path/to/source.txt /path/to/link.txt"));
			expect(cli.$).toEqual({
				mode: "",
				links: [],
				unlinks: [],
			});
			expect(cli._).toEqual(["link", "/path/to/source.txt", "/path/to/link.txt"]);
			expect(cli["--"]).toEqual([]);
		});
	});
});
