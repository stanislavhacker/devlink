/* eslint-disable @typescript-eslint/no-non-null-assertion */
import "jasmine";
import * as React from "react";
import { Box } from "ink";

import { transformErrorCode } from "../../src/app/exit";
import { ErrorReason, ErrorCode } from "../../src/model";

describe("exit", () => {
	function createExitReason(exitCode: number): ErrorReason {
		return {
			exitCode,
			code: ErrorCode.MISSING_FILES,
			component: <Box></Box>,
		};
	}

	describe("transformErrorCode", () => {
		it("no current code, and provide 1 exit code", () => {
			const { exitCode } = transformErrorCode(undefined)(createExitReason(1));
			expect(exitCode).toBe(1);
		});

		it("current code is 1, and provide 2 exit code", () => {
			const { exitCode } = transformErrorCode(1)(createExitReason(2));
			expect(exitCode).toBe(1);
		});

		it("current code is 2, and provide 1 exit code", () => {
			const { exitCode } = transformErrorCode(2)(createExitReason(1));
			expect(exitCode).toBe(1);
		});
	});
});
