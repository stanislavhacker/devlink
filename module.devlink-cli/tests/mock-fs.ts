// eslint-disable-next-line @typescript-eslint/ban-ts-comment
// @ts-ignore
process.env.MEMFS_DONT_WARN = true;

import * as fs from "fs";
import * as path from "path";
import * as memfs from "memfs";

import "jasmine";

export interface FsMock {
	[key: string]: string | Buffer | FsMock | FsDir | FsFile;
}
export interface FsDir {
	type: FsType.Dir;
	content: FsMock;
}
export interface FsFile {
	type: FsType.File;
	content: string | Buffer;
}
enum FsType {
	File = "file",
	Dir = "dir",
}

export function mockFs(options: FsMock): () => void {
	const keys = Object.keys(fs).filter(
		(name) =>
			typeof fs[name] === "function" && name[0].toLowerCase() === name[0] && name[0] !== "_"
	) as Array<string>;

	const vol = new memfs.Volume();
	const mfs = memfs.createFsFromVolume(vol);
	writeStore(mfs, options);

	const servantDir = path.join(__dirname, "../../");

	keys.forEach((key) => {
		// eslint-disable-next-line @typescript-eslint/no-explicit-any
		spyOn(fs, key as any).and.callFake((a, b, c, d, e) => {
			if (typeof a === "string" && a.indexOf(servantDir) === 0) {
				return fs[key].and.originalFn.apply(
					null,
					createParams.apply(null, [a, b, c, d, e])
				);
			}
			if (typeof a === "number") {
				return fs[key].and.originalFn.apply(
					null,
					createParams.apply(null, [a, b, c, d, e])
				);
			}
			return mfs[key].apply(null, createParams.apply(null, [a, b, c, d, e]));
		});
	});

	return async () => {
		vol.reset();
		await wait(100);
	};
}

function createParams(...args) {
	while (args.length > 0 && args[args.length - 1] === undefined) {
		args.pop();
	}
	return args;
}

export function file(props: Partial<FsFile>): FsFile {
	return {
		content: props.content || "",
		type: FsType.File,
	};
}

export function dir(props: Partial<FsDir>): FsDir {
	return {
		content: props.content || {},
		type: FsType.Dir,
	};
}

//CREATE

function wait(time: number): Promise<void> {
	return new Promise((resolve) => {
		setTimeout(resolve, time);
	});
}

function writeStore(fs: memfs.IFs, options: FsMock) {
	createLevel("", fs, options);
}

function createLevel(parent: string, fs: memfs.IFs, options: FsMock) {
	Object.keys(options).forEach((key) => {
		const fullPath = normalize(path.join(parent, key));
		const item = options[key] as string | Buffer | FsMock | FsDir | FsFile;

		//FILE
		if (typeof item === "string" || item instanceof Buffer) {
			fs.writeFileSync(fullPath, item);
			return;
		}
		if (item && typeof item === "object" && item.type === FsType.File) {
			const file = item as FsFile;
			fs.writeFileSync(fullPath, file.content);
			return;
		}
		//DIRECTORY
		if (item && typeof item === "object" && item.type === FsType.Dir) {
			const directory = item as FsDir;
			fs.mkdirSync(fullPath, { recursive: true });
			createLevel(fullPath, fs, directory.content);
			return;
		}
		if (item && typeof item === "object") {
			fs.mkdirSync(fullPath, { recursive: true });
			createLevel(fullPath, fs, item as FsMock);
			return;
		}
	});
}

function normalize(p: string) {
	return path.normalize(p).replace(/\\/g, "/");
}
