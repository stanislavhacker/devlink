import * as O from "fp-ts/Option";
import * as E from "fp-ts/Either";
import * as F from "fp-ts/function";

export function eitherToValue<R, A>(result: E.Either<R, A>) {
	return F.pipe(
		result,
		E.fold(
			() => null,
			(res) => res
		)
	);
}
export function eitherToError<R, A>(result: E.Either<R, A>) {
	return F.pipe(
		result,
		E.swap,
		E.fold(
			() => null,
			(res) => res
		)
	);
}

export function optionAsValue<T>(value: O.Option<T>): T | null {
	return F.pipe(value, O.fold(F.constant(null), F.identity));
}

export function clearOutput(text = ""): Array<string> {
	return text
		.replace(
			// eslint-disable-next-line no-control-regex
			/[\u001b\u009b][[()#;?]*(?:[0-9]{1,4}(?:;[0-9]{0,4})*)?[0-9A-ORZcf-nqry=><]/g,
			""
		)
		.split("\n")
		.map((line) => line.trimEnd());
}
