/* eslint-disable @typescript-eslint/no-non-null-assertion */

import * as E from "fp-ts/Either";
import * as path from "path";

import { action } from "../../src/actions";
import { LinkResults } from "../../src/model";
import { createStore } from "../store";
import { eitherToValue, eitherToError } from "../utils";

describe("link", () => {
	let clean;

	beforeEach(() => {
		clean = createStore();
	});

	it("create link with ok", () => {
		const result = action({
			cwd: "/",
			mode: "link",
			files: [{ on: "/data/sources/source.txt", to: "/data/links/link.txt", errors: [] }],
		});
		expect(E.isRight(result)).toBeTrue();

		const value = eitherToValue(result) as LinkResults;
		expect(value.mode).toBe("link");
		expect(value.errors.length).toBe(0);

		expect(E.isRight(value.results[0].result)).toBeTrue();
		const res = eitherToValue(value.results[0].result)!;
		expect(res).toEqual({
			on: path.normalize("/data/sources/source.txt"),
			to: path.normalize("/data/links/link.txt"),
			original: path.normalize("/data/sources/@@linked#source.txt"),
		});
	});

	it("create link with ok and cwd", () => {
		const result = action({
			cwd: "/data/sources",
			mode: "link",
			files: [{ on: "./source.txt", to: "./link.txt", errors: [] }],
		});
		expect(E.isRight(result)).toBeTrue();

		const value = eitherToValue(result) as LinkResults;
		expect(value.mode).toBe("link");
		expect(value.errors.length).toBe(0);

		expect(E.isRight(value.results[0].result)).toBeTrue();
		const res = eitherToValue(value.results[0].result)!;
		expect(res).toEqual({
			on: path.normalize("/data/sources/source.txt"),
			to: path.normalize("/data/sources/link.txt"),
			original: path.normalize("/data/sources/@@linked#source.txt"),
		});
	});

	it("create link with fail inside", () => {
		const result = action({
			cwd: "/",
			mode: "link",
			files: [{ on: "/data/sources/source2.txt", to: "/data/links/link2.txt", errors: [] }],
		});

		const value = eitherToValue(result) as LinkResults;
		expect(value.mode).toBe("link");
		expect(value.errors.length).toBe(0);

		expect(E.isLeft(value.results[0].result)).toBeTrue();
		const res = eitherToError(value.results[0].result)!;
		expect(res).toEqual({
			name: "Call in 'lstatSync' failed.",
			description:
				"There is some error occurred when calling native method from nodejs fs module.",
			type: "TYPE_FILE_ERROR",
			original: jasmine.any(Error),
		});
	});

	afterEach(() => {
		clean();
	});
});
