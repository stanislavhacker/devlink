/* eslint-disable @typescript-eslint/no-non-null-assertion */

import * as E from "fp-ts/Either";
import * as path from "path";
import { functional } from "devlink-core";

import { action } from "../../src/actions";
import { UnlinkResults } from "../../src/model";
import { createStore } from "../store";
import { eitherToValue, eitherToError } from "../utils";

describe("link", () => {
	let clean;

	beforeEach(() => {
		clean = createStore();
	});

	it("create unlink with ok", () => {
		functional.link("/data/sources/source.txt", "/data/links/link.txt");

		const result = action({
			cwd: "/",
			mode: "unlink",
			files: [{ on: "/data/sources/source.txt", to: "/data/links/link.txt", errors: [] }],
		});
		expect(E.isRight(result)).toBeTrue();

		const value = eitherToValue(result) as UnlinkResults;
		expect(value.mode).toBe("unlink");
		expect(value.errors.length).toBe(0);

		expect(E.isRight(value.results[0].result)).toBeTrue();
		const res = eitherToValue(value.results[0].result)!;
		expect(res).toEqual({
			on: path.normalize("/data/sources/source.txt"),
		});
	});

	it("create unlink with ok and cwd", () => {
		functional.link("/data/sources/source.txt", "/data/links/link.txt");

		const result = action({
			cwd: "/data/sources",
			mode: "unlink",
			files: [{ on: "./source.txt", to: "./link.txt", errors: [] }],
		});
		expect(E.isRight(result)).toBeTrue();

		const value = eitherToValue(result) as UnlinkResults;
		expect(value.mode).toBe("unlink");
		expect(value.errors.length).toBe(0);

		expect(E.isRight(value.results[0].result)).toBeTrue();
		const res = eitherToValue(value.results[0].result)!;
		expect(res).toEqual({
			on: path.normalize("/data/sources/source.txt"),
		});
	});

	it("create unlink with fail inside", () => {
		const result = action({
			cwd: "/",
			mode: "unlink",
			files: [{ on: "/data/sources/source.txt", to: "/data/links/link.txt", errors: [] }],
		});

		const value = eitherToValue(result) as UnlinkResults;
		expect(value.mode).toBe("unlink");
		expect(value.errors.length).toBe(0);

		expect(E.isLeft(value.results[0].result)).toBeTrue();
		const res = eitherToError(value.results[0].result)!;
		expect(res).toEqual({
			name: "No symlink exists on path.",
			description: jasmine.any(String),
			type: "NO_SYMLINK_ON_PATH",
			original: undefined,
		});
	});

	afterEach(() => {
		clean();
	});
});
