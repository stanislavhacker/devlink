import * as F from "fp-ts/function";
import * as A from "fp-ts/Array";
import * as S from "fp-ts/Separated";
import { functional } from "devlink-core";

import { ActionRequest, LINK_MODE, LinkResults } from "../model";

import { absolute } from "./utils";

export function link({ cwd, files }: Omit<ActionRequest, "mode">): LinkResults {
	const { link } = functional;

	return F.pipe(
		files,
		A.partition(({ errors }) => errors.length === 0),
		S.map((valid) =>
			F.pipe(
				valid,
				A.map((request) => ({
					result: link(absolute(cwd, request.on), absolute(cwd, request.to)),
					request,
				}))
			)
		),
		(data) =>
			({
				mode: LINK_MODE,
				errors: F.pipe(S.left(data)),
				results: F.pipe(S.right(data)),
			} as LinkResults)
	);
}
