import * as E from "fp-ts/Either";
import * as F from "fp-ts/function";

import { ActionRequest, ActionResults, ErrorReason, ERRORS, foldMode } from "../model";
import { error } from "../components";

import { link } from "./link";
import { unlink } from "./unlink";

export function action({ cwd, mode, files }: ActionRequest): E.Either<ErrorReason, ActionResults> {
	return F.pipe(
		mode,
		foldMode<E.Either<ErrorReason, ActionResults>>({
			whenLink: () => E.right(link({ cwd, files })),
			whenUnlink: () => E.right(unlink({ cwd, files })),
			whenUnknown: () => F.pipe(E.left(error(ERRORS.MISSING_MODE))),
		})
	);
}
