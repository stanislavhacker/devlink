import * as F from "fp-ts/function";
import * as A from "fp-ts/Array";
import * as S from "fp-ts/Separated";
import { functional } from "devlink-core";

import { ActionRequest, UNLINK_MODE, UnlinkResults } from "../model";

import { absolute } from "./utils";

export function unlink({ cwd, files }: Omit<ActionRequest, "mode">): UnlinkResults {
	const { unlink } = functional;

	return F.pipe(
		files,
		A.partition(({ errors }) => errors.length === 0),
		S.map((valid) =>
			F.pipe(
				valid,
				A.map((request) => ({ result: unlink(absolute(cwd, request.on)), request }))
			)
		),
		(data) =>
			({
				mode: UNLINK_MODE,
				errors: F.pipe(S.left(data)),
				results: F.pipe(S.right(data)),
			} as UnlinkResults)
	);
}
