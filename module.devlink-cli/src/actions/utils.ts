import * as path from "path";

export function absolute(cwd: string, file: string) {
	return path.join(cwd, file);
}
