import { render } from "ink";

import { app } from "./app";
import { get, parse, usedConfig } from "./cli";

const data = get(process);

render(
	app({
		...parse(data.$, data._),
		config: usedConfig(data.config),
		env: usedConfig(data.env),
	})
);
