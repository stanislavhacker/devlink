import * as React from "react";
import { Box, Text } from "ink";

export const HeaderSize = 10;

export const Header: React.VFC = () => {
	return (
		<Box marginLeft={1} marginRight={2} flexDirection="row" minWidth={HeaderSize - 3}>
			<Text italic color="gray">
				devlink
			</Text>
		</Box>
	);
};
