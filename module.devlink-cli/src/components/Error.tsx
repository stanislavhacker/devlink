import * as React from "react";
import { Box, Text } from "ink";
import * as Divider from "ink-divider";

import * as O from "fp-ts/Option";
import * as F from "fp-ts/function";
import * as E from "fp-ts/Either";
import * as A from "fp-ts/Array";

import { ErrorDefinition, ErrorReason, EXIT_CODES } from "../model";

import { Header, HeaderSize } from "./Header";

type ErrorProps = Omit<ErrorDefinition, "code"> & {
	code?: ErrorDefinition["code"];
	id?: string;
	original?: Error;
};

export const Error: React.VFC<ErrorProps> = ({ title, description, id, code, tip, original }) => {
	const cd = code || id;

	return (
		<>
			<Box flexDirection="row">
				<Header />
				<Box justifyContent="flex-start" minWidth={8}>
					<Text bold color="whiteBright" backgroundColor="red">
						[ERROR]
					</Text>
					<Text> </Text>
				</Box>
				<Box justifyContent="flex-start">
					<Text>
						<Text bold color="red">
							{title}
						</Text>
						<Text>: </Text>
						<Text italic color="redBright">
							{description}
						</Text>
						{cd && (
							<Text italic color="gray">
								{" "}
								as {cd}
							</Text>
						)}
					</Text>
				</Box>
			</Box>
			{tip && (
				<Box flexDirection="row" marginLeft={HeaderSize}>
					<Box justifyContent="flex-start" minWidth={5}>
						<Text color="blueBright" bold>
							TIP
						</Text>
						<Text>: </Text>
					</Box>
					<Text color="white">{tip}</Text>
				</Box>
			)}
			{original && (
				<Box flexDirection="column" marginLeft={1} marginTop={1}>
					<Divider titleColor="magentaBright" title="Original error" width={30} />
					<Box>
						<Text italic color="white">
							{original.stack}
						</Text>
					</Box>
				</Box>
			)}
		</>
	);
};

export function errorOption<T>(
	value: O.Option<T>,
	err: ErrorDefinition,
	original?: Error
): E.Either<ErrorReason, T> {
	return F.pipe(
		value,
		E.fromOption(() => error(err, original))
	);
}

export function errorFromNone<T>(
	value: O.Option<T>,
	err: ErrorDefinition
): O.Option<ErrorDefinition> {
	return F.pipe(
		value,
		O.match(
			() => O.some(err),
			() => O.none
		)
	);
}

export function errorFromSome<T>(
	value: O.Option<T>,
	err: ErrorDefinition
): O.Option<ErrorDefinition> {
	return F.pipe(
		value,
		O.match(
			() => O.none,
			() => O.some(err)
		)
	);
}

export function errorsFromOptions(errs: O.Option<ErrorDefinition>[]): ErrorDefinition[] {
	return F.pipe(
		errs,
		A.map((a) =>
			F.pipe(
				a,
				O.matchW(() => null, F.identity)
			)
		),
		A.filter(Boolean),
		(a) => a as Array<ErrorDefinition>
	);
}

export function error(err: ErrorDefinition, original?: Error): ErrorReason {
	return {
		component: <Error {...err} original={original} />,
		code: err.code,
		exitCode: EXIT_CODES[err.code],
	};
}
