import * as React from "react";
import { LinkError, UnlinkError } from "devlink-core";

import { Modes, OneLinkResult, OneUnlinkResult } from "../../model";

import { Fail } from "./Fail";

type ErrorProps = {
	mode: Modes;
	error: UnlinkError | LinkError;
	request: OneLinkResult["request"] | OneUnlinkResult["request"];
};

export const Error: React.VFC<ErrorProps> = ({ mode, error, request }) => {
	const { on, to } = request;
	const { name, description, type, original } = error;

	return (
		<Fail
			mode={mode}
			on={on}
			to={to}
			title={name}
			description={description}
			id={type}
			original={original}
		/>
	);
};
