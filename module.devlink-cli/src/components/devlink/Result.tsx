import * as React from "react";
import { Box } from "ink";
import { LinkResult, UnlinkResult } from "devlink-core";

import { LINK_MODE, Modes, OneLinkResult, OneUnlinkResult, UNLINK_MODE } from "../../model";
import { Header } from "../Header";

import { Link, Unlink } from "./Actions";

type ErrorProps = {
	mode: Modes;
	request: OneLinkResult["request"] | OneUnlinkResult["request"];
	result: UnlinkResult | LinkResult;
};

export const Results: React.VFC<ErrorProps> = ({ mode, request }) => {
	if (mode === LINK_MODE) {
		const { on, to } = request as OneLinkResult["request"];

		return (
			<Box flexDirection="row">
				<Header />
				<Link on={on} to={to} />
			</Box>
		);
	}
	if (mode === UNLINK_MODE) {
		const { on } = request as OneUnlinkResult["request"];

		return (
			<Box flexDirection="row">
				<Header />
				<Unlink on={on} />
			</Box>
		);
	}
	return null;
};
