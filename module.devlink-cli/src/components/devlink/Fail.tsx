import * as React from "react";
import { Box, Text } from "ink";

import { LINK_MODE, UNLINK_MODE, Modes, ErrorCode } from "../../model";
import { Error as ErrorComponent } from "../Error";
import { Header } from "../Header";

import { Link, Unlink } from "./Actions";

type FailProps = {
	mode: Modes;
	on: string;
	to: string;
	title: string;
	description: string;
	id?: string;
	code?: ErrorCode;
	tip?: string;
	original?: Error;
};

export const Fail: React.VFC<FailProps> = ({
	mode,
	on,
	to,
	title,
	description,
	id,
	code,
	tip,
	original,
}) => {
	return (
		<Box flexDirection="column">
			<Box flexDirection="row">
				<Header />
				<Box justifyContent="flex-start">
					<Text bold color="whiteBright" backgroundColor="red">
						Operation failed
					</Text>
					<Text> </Text>
				</Box>
				{mode === LINK_MODE && (
					<>
						<Text color="white" italic>
							Linking{" "}
						</Text>
						<Link on={on} to={to} />
					</>
				)}
				{mode === UNLINK_MODE && (
					<>
						<Text color="white" italic>
							Unlinking{" "}
						</Text>
						<Unlink on={on} />
					</>
				)}
			</Box>
			<ErrorComponent
				title={title}
				description={description}
				id={id}
				code={code}
				tip={tip}
				original={original}
			/>
		</Box>
	);
};
