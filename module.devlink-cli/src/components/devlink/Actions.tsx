import * as React from "react";
import { Box, Text } from "ink";

type LinkProps = {
	on: string;
	to: string;
};

export const Link: React.VFC<LinkProps> = ({ on, to }) => {
	return (
		<Box flexDirection="row">
			<Text color="green">{" +sl "}</Text>
			<Text color="blueBright">{on}</Text>
			<Text color="white">{" => "}</Text>
			<Text color="green">{to}</Text>
		</Box>
	);
};

type UnlinkProps = {
	on: string;
};

export const Unlink: React.VFC<UnlinkProps> = ({ on }) => {
	return (
		<Box flexDirection="row">
			<Text color="red">{" -sl "}</Text>
			<Text color="blueBright">{on}</Text>
		</Box>
	);
};
