import * as React from "react";
import { Box, Text } from "ink";

import { EXIT_CODES, ActionResults, ErrorDefinition, Modes } from "../model";

import { Fail } from "./devlink/Fail";

type ErrorProps = {
	mode: Modes;
	errors: ActionResults["errors"];
};

export const Errors: React.VFC<ErrorProps> = ({ mode, errors }) => {
	if (errors.length === 0) {
		return null;
	}

	return (
		<Box flexDirection="column" marginTop={1}>
			<Text color="red" bold>
				Invalid files
			</Text>
			{errors.map(({ on, to, errors }, i) => {
				const { title, description, code, tip } = selectMostImportantError(errors);
				return (
					<Fail
						mode={mode}
						on={on}
						to={to}
						title={title}
						description={description}
						code={code}
						tip={tip}
						key={i}
					/>
				);
			})}
		</Box>
	);
};

function selectMostImportantError(arr: ErrorDefinition[]) {
	return arr.sort((a, b) => EXIT_CODES[b.code] - EXIT_CODES[a.code])[0];
}
