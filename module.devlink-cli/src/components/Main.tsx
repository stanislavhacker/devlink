import * as React from "react";
import { Box, Text } from "ink";

import * as F from "fp-ts/function";
import * as O from "fp-ts/Option";
import * as A from "fp-ts/Array";
import * as E from "fp-ts/Either";

import { ActionRequest, ActionResults, Modes, OneLinkResult, OneUnlinkResult } from "../model";

import { Header } from "./Header";
import { Error } from "./devlink/Error";
import { Results } from "./devlink/Result";
import { Errors } from "./Errors";

type MainProps = ActionResults & {
	config: O.Option<string>;
	env: O.Option<string>;
};

const Row1Size = 15;

export const Main: React.VFC<MainProps> = ({ mode, results, errors, config, env }) => {
	return (
		<>
			<Box flexDirection="row">
				<Header />
				<Box flexDirection="row">
					<Text italic color="green">
						{mode}
					</Text>
				</Box>
			</Box>
			{renderOption(config, (value) => (
				<Box flexDirection="row">
					<Header />
					<Box minWidth={Row1Size}>
						<Text italic color="blueBright">
							[config]
						</Text>
					</Box>
					<Box>
						<Text italic color="white">
							{value}
						</Text>
					</Box>
				</Box>
			))}
			{renderOption(env, (value) => (
				<Box flexDirection="row">
					<Header />
					<Box minWidth={Row1Size}>
						<Text italic color="blueBright">
							[environment]
						</Text>
					</Box>
					<Box>
						<Text italic color="white">
							{value}
						</Text>
					</Box>
				</Box>
			))}
			{renderResults(mode, results)}
			{renderErrors(mode, errors)}
		</>
	);
};

export function main(results: ActionResults, config: O.Option<string>, env: O.Option<string>) {
	return <Main {...results} config={config} env={env} />;
}

function renderOption<T>(value: O.Option<T>, comp: (value: T) => JSX.Element) {
	return F.pipe(
		value,
		O.match(() => null, comp)
	);
}

function renderResults(mode: Modes, data: (OneLinkResult | OneUnlinkResult)[]) {
	return F.pipe(
		data,
		A.mapWithIndex((i, { result, request }) =>
			F.pipe(
				result,
				E.matchW(
					(e) => <Error key={i} mode={mode} request={request} error={e} />,
					(r) => <Results key={i} mode={mode} request={request} result={r} />
				)
			)
		)
	);
}

function renderErrors(mode: Modes, errors: ActionRequest["files"]) {
	return <Errors mode={mode} errors={errors} />;
}
