import * as F from "fp-ts/function";
import * as E from "fp-ts/Either";
import * as fs from "fs";

import { ErrorReason, ERRORS } from "../model";
import { error } from "../components";

export function exists(on: string): E.Either<ErrorReason, boolean> {
	return F.pipe(
		E.tryCatch(
			() => existsSync(on),
			(e: Error) => error(ERRORS.INPURE_EXISTS_CALL_FAILED, e)
		)
	);
}

function existsSync(on: string): boolean {
	return fs.existsSync(on);
}
