import * as F from "fp-ts/function";
import * as E from "fp-ts/Either";
import * as fs from "fs";

export type FileType = "dir" | "file" | "link" | "unknown";

export function type(on: string): E.Either<never, FileType> {
	return F.pipe(
		E.tryCatch(
			() => fileTypeSync(on),
			() => null
		),
		E.orElseW(() => E.right("unknown" as FileType))
	);
}

function fileTypeSync(on: string) {
	const stat = fs.lstatSync(on);

	if (stat.isSymbolicLink()) {
		return "link";
	}
	if (stat.isFile()) {
		return "file";
	}
	if (stat.isDirectory()) {
		return "dir";
	}
	return "unknown";
}
