import Process = NodeJS.Process;
import * as path from "path";
import { envfull, EnvfullVars } from "envfull";

import * as A from "fp-ts/Array";
import * as ST from "fp-ts/string";
import * as O from "fp-ts/Option";
import * as F from "fp-ts/function";
import * as S from "fp-ts/Semigroup";

import { AppProps } from "./app";
import { LINK_MODE, UNLINK_MODE, Modes, foldMode } from "./model";

export const CONFIG_FILE_NAME = ".link.json";

export type LinkCli = {
	mode: string;
	links: Array<string>;
	unlinks: Array<string>;
};

export function get(process: Process): EnvfullVars<LinkCli> {
	return envfull<LinkCli>(process, {
		arrays: [],
		defaults: {
			mode: "",
			links: [],
			unlinks: [],
		},
		aliases: {},
	})(path.join(process.cwd(), CONFIG_FILE_NAME));
}

export function parse(
	obj: EnvfullVars["$"],
	rest: EnvfullVars["_"]
): Pick<AppProps, "cwd" | "files" | "mode"> {
	const parsedRest = F.pipe(rest, parseRest);
	const parsedObj = F.pipe(obj, parseObj);

	return F.pipe(
		[parsedObj, parsedRest],
		(data) =>
			({
				mode: mergeOptions(data, "mode"),
				links: mergeOptions(data, "links"),
				unlinks: mergeOptions(data, "unlinks"),
			} as ParsedCli),
		({ mode, links, unlinks }) => ({
			mode,
			cwd: process.cwd(),
			files: F.pipe(
				mode,
				O.map((mode: Modes) => {
					return F.pipe(
						mode,
						foldMode({
							whenLink: () => links,
							whenUnlink: () => unlinks,
							whenUnknown: () => O.none,
						})
					);
				}),
				O.flatten
			),
		})
	);
}

type ParsedCli = {
	mode: O.Option<Modes>;
	links: O.Option<string[]>;
	unlinks: O.Option<string[]>;
};

function parseObj(obj: EnvfullVars<LinkCli>["$"]): ParsedCli {
	return F.pipe(
		obj,
		ensureArray("links"),
		ensureArray("unlinks"),
		toLowerCase,
		({ mode, links, unlinks }) => ({
			links: F.pipe(
				links,
				O.fromPredicate((a) => a.length > 0)
			),
			unlinks: F.pipe(
				unlinks,
				O.fromPredicate((a) => a.length > 0)
			),
			mode: getMode([mode] as Array<Modes>),
		})
	);
}

function parseRest(rest: EnvfullVars["_"]): ParsedCli {
	return F.pipe(
		rest,
		A.map((item) => item.toString().toLowerCase()),
		(a) => ({
			links: getFiles(a),
			unlinks: getFiles(a),
			mode: getMode(a as Array<Modes>),
		})
	);
}

const getFiles = (arr: string[]) => {
	return F.pipe(
		arr,
		A.filter(isFile),
		O.fromPredicate((a) => a.length > 0)
	);
};

const getMode = (arr: Array<Modes>) => {
	return F.pipe(arr, A.filter(isMode), A.findFirst(Boolean));
};

export function usedConfig(data: EnvfullVars["config"] | EnvfullVars["env"]) {
	return F.pipe(
		[data],
		A.findFirst((a) => a.used),
		O.map((a) => a.path)
	);
}

const isMode = (item) => [LINK_MODE, UNLINK_MODE].indexOf(item) >= 0;
const isFile = (item) => !isMode(item);

const toLowerCase = ({ mode = "", links = [], unlinks = [] }: Partial<LinkCli>) => {
	return {
		mode: mode.toLowerCase(),
		links: links?.map(ST.toLowerCase),
		unlinks: unlinks?.map(ST.toLowerCase),
	};
};

const mergeOptions = <T extends Record<string, O.Option<unknown>>, R>(
	objs: T[],
	key: keyof (typeof objs)[number]
): O.Option<R> => {
	const M = O.getMonoid(S.last());
	return F.pipe(
		objs,
		A.map((a) => a[key] as O.Option<R>),
		A.reduceRight(O.none, M.concat)
	);
};
const ensureArray = <T extends Record<string, unknown>>(key: keyof T) => {
	return (data: T) => {
		return { ...data, [key]: convertToArray(data[key]) };
	};
};
const convertToArray = <T>(data: T[] | T) => {
	return F.pipe(
		(Array.isArray(data) ? data : data !== undefined ? [data] : []) as string[],
		A.map((data) => data.split(",")),
		A.flatten
	);
};
