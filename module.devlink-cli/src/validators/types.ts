import { Modes } from "../model";

export type ValidInputs = {
	cwd: string;
	mode: Modes;
	files: Array<string>;
};
