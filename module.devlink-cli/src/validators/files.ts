import * as F from "fp-ts/function";
import * as E from "fp-ts/Either";
import * as O from "fp-ts/Option";
import * as A from "fp-ts/Array";

import { errorFromNone, errorFromSome, errorOption, errorsFromOptions } from "../components";
import { ActionRequest, ErrorReason, ERRORS } from "../model";

import { ValidInputs } from "./types";
import {
	convertSeparatedFilesToLinkedFiles,
	convertToLinkedFiles,
	convertToSeparatedFiles,
	createFileInfo,
	FileInfo,
} from "./func";

export function filledFiles<T>(files: O.Option<string[]>) {
	return (data: T) =>
		F.pipe(
			errorOption(files, ERRORS.MISSING_FILES),
			E.map((files) => ({ ...data, files }))
		);
}

export function linkFiles(inputs: ValidInputs): E.Either<ErrorReason, ActionRequest> {
	return F.pipe(
		E.right(inputs),
		E.map(({ files, ...rest }) =>
			F.pipe(
				files,
				A.map(createFileInfo),
				E.sequenceArray,
				E.map((fileInfos: Array<FileInfo>) =>
					F.pipe(
						convertToSeparatedFiles(files, fileInfos),
						convertSeparatedFilesToLinkedFiles
					)
				),
				E.map((linkedFiles) =>
					F.pipe(
						linkedFiles,
						A.map(({ on, to, file, link, type }) => {
							const errors = errorsFromOptions([
								errorFromNone(file, ERRORS.FILE_NOT_EXISTS),
								errorFromSome(link, ERRORS.FILE_IS_SYMLINK_ALREADY),
								errorFromNone(type, ERRORS.FILES_INVALID_TYPE),
							]);
							return { on, to, errors } as ActionRequest["files"][number];
						})
					)
				),
				E.map((files) => ({ files, ...rest }))
			)
		),
		E.flattenW
	);
}

export function unlinkFiles(inputs: ValidInputs): E.Either<ErrorReason, ActionRequest> {
	return F.pipe(
		E.right(inputs),
		E.map(({ files, ...rest }) =>
			F.pipe(
				files,
				A.map(createFileInfo),
				E.sequenceArray,
				E.map((fileInfos: Array<FileInfo>) => convertToLinkedFiles(files, fileInfos)),
				E.map((linkedFiles) =>
					F.pipe(
						linkedFiles,
						A.map(({ on, to, file, link, type }) => {
							const errors = errorsFromOptions([
								errorFromNone(file, ERRORS.FILE_NOT_EXISTS),
								errorFromNone(link, ERRORS.FILE_IS_NOT_SYMLINK),
								errorFromNone(type, ERRORS.FILES_INVALID_TYPE),
							]);
							return { on, to, errors } as ActionRequest["files"][number];
						})
					)
				),
				E.map((files) => ({ files, ...rest }))
			)
		),
		E.flattenW
	);
}
