import * as S from "fp-ts/Separated";
import * as F from "fp-ts/function";
import * as A from "fp-ts/Array";
import * as E from "fp-ts/Either";
import * as O from "fp-ts/Option";
import { functional } from "devlink-core";

import { ErrorReason, ERRORS } from "../../model";
import { exists, type } from "../../inpure";
import { error } from "../../components";

export type ToFile = { to: string } & FileInfo;
export type OnFile = { on: string } & FileInfo;

export type SeparatedFiles = S.Separated<ToFile[], OnFile[]>;
export const convertToSeparatedFiles = (
	files: Array<string>,
	exists: Array<FileInfo>
): SeparatedFiles => {
	return F.pipe(
		A.zip(files, exists),
		A.mapWithIndex((index, data) => [index, ...data] as [number, string, FileInfo]),
		A.partitionWithIndex((i) => i % 2 === 0),
		S.bimap(
			(to) =>
				F.pipe(
					to,
					A.map(([, to, fileInfo]) => ({ to, ...fileInfo }))
				),
			(on) =>
				F.pipe(
					on,
					A.map(([, on, fileInfo]) => ({ on, ...fileInfo }))
				)
		)
	);
};

export type LinkedFile = ToFile & OnFile;
export const convertSeparatedFilesToLinkedFiles = (data: SeparatedFiles): LinkedFile[] => {
	return F.pipe(
		S.left(data),
		A.zip(S.right(data)),
		A.map(([to, on]) => composeLinkedFile(on, to))
	);
};

export const convertToLinkedFiles = (
	files: Array<string>,
	exists: Array<FileInfo>
): LinkedFile[] => {
	return F.pipe(
		A.zip(files, exists),
		A.map(([on, fileInfo]) => ({ on, to: on, ...fileInfo }))
	);
};

export type FileType = "dir" | "file" | "link";
export type FileInfo = {
	file: O.Option<true>;
	link: O.Option<true>;
	type: O.Option<FileType>;
};
export const createFileInfo = (file: string): E.Either<ErrorReason, FileInfo> => {
	return F.pipe(
		E.Do,
		E.chainW((data) =>
			F.pipe(
				file,
				exists,
				E.map((fileExists) => ({
					...data,
					file: O.fromPredicate(() => fileExists)(true as const),
				}))
			)
		),
		E.chainW((data) =>
			F.pipe(
				file,
				linked,
				E.map((linkExists) => ({
					...data,
					link: O.fromPredicate(() => linkExists)(true as const),
				}))
			)
		),
		E.chainW((data) =>
			F.pipe(
				file,
				type,
				E.map((type) => ({
					...data,
					type: O.fromPredicate(() => type !== "unknown")(type as FileType),
				}))
			)
		)
	);
};

//utils

function linked(file: string) {
	return F.pipe(
		file,
		functional.linked,
		E.map(({ exists }) => exists),
		E.orElseW(({ name, description, original }) =>
			E.left(error({ ...ERRORS.DEVLINK_PACKAGE_ERROR, title: name, description }, original))
		)
	);
}

function composeLinkedFile(on: OnFile, to: ToFile): LinkedFile {
	return F.pipe({ on: on.on, to: to.to }, (data) => ({
		...data,
		link: F.pipe([on.link, to.link], A.filter(O.isSome), (a) =>
			F.pipe(
				true as const,
				O.fromPredicate(() => a.length > 0)
			)
		),
		file: F.pipe([on.file, to.file], A.filter(O.isSome), (a) =>
			F.pipe(
				true as const,
				O.fromPredicate(() => a.length === 2)
			)
		),
		type: determineFileType(on.type, to.type),
	}));
}

function determineFileType(a: LinkedFile["type"], b: LinkedFile["type"]) {
	return F.pipe(
		[a, b],
		([a, b]) => [
			F.pipe(
				a,
				O.fold(() => null, F.identity)
			),
			F.pipe(
				b,
				O.fold(() => null, F.identity)
			),
		],
		([a, b]) =>
			F.pipe(
				a,
				O.fromPredicate(() => Boolean(a === b && a)),
				O.map((a) => a as FileType)
			)
	);
}
