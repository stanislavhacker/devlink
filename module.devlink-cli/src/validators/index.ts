import * as E from "fp-ts/Either";
import * as F from "fp-ts/function";

import { error } from "../components";
import { ERRORS, foldMode, ErrorReason, CliInputs, ActionRequest } from "../model";

import { ValidInputs } from "./types";
import { filledMode, linkMode, unlinkMode } from "./mode";
import { filledFiles, linkFiles, unlinkFiles } from "./files";

export function validateInputs({
	cwd,
	mode,
	files,
}: CliInputs): E.Either<ErrorReason, ValidInputs> {
	return F.pipe(E.right({ cwd }), E.chainW(filledMode(mode)), E.chainW(filledFiles(files)));
}

export function validateMode({
	cwd,
	mode,
	files,
}: ValidInputs): E.Either<ErrorReason, ValidInputs> {
	return F.pipe(
		mode,
		foldMode({
			whenLink: () => linkMode({ cwd, mode, files }),
			whenUnlink: () => unlinkMode({ cwd, mode, files }),
			whenUnknown: () => F.pipe(E.left(error(ERRORS.MISSING_MODE))),
		})
	);
}

export function validateFiles({
	cwd,
	mode,
	files,
}: ValidInputs): E.Either<ErrorReason, ActionRequest> {
	return F.pipe(
		mode,
		foldMode({
			whenLink: () => linkFiles({ cwd, mode, files }),
			whenUnlink: () => unlinkFiles({ cwd, mode, files }),
			whenUnknown: () => F.pipe(E.left(error(ERRORS.MISSING_MODE))),
		})
	);
}

export function validate(input: CliInputs): E.Either<ErrorReason, ActionRequest> {
	return F.pipe(
		E.right(input),
		E.chainW((data) => validateInputs(data)),
		E.chainW((data) => validateMode(data)),
		E.chainW((data) => validateFiles(data))
	);
}
