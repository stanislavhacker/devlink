import * as F from "fp-ts/function";
import * as E from "fp-ts/Either";
import * as O from "fp-ts/Option";

import { ERRORS, Modes } from "../model";
import { errorOption, error } from "../components";

import { ValidInputs } from "./types";

export function filledMode<T>(mode: O.Option<Modes>) {
	return (data: T) =>
		F.pipe(
			errorOption(mode, ERRORS.MISSING_MODE),
			E.map((mode) => ({ ...data, mode }))
		);
}

export function linkMode({ cwd, mode, files }: ValidInputs) {
	return F.pipe(
		{ cwd, mode, files },
		E.fromPredicate(
			({ files }) => files.length % 2 === 0 && files.length >= 2,
			() => error(ERRORS.INVALID_LINK_FILES_COUNT)
		)
	);
}

export function unlinkMode({ cwd, mode, files }: ValidInputs) {
	return F.pipe(
		{ cwd, mode, files },
		E.fromPredicate(
			({ files }) => files.length >= 1,
			() => error(ERRORS.INVALID_UNLINK_FILES_COUNT)
		)
	);
}
