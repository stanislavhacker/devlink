import * as O from "fp-ts/Option";
import * as E from "fp-ts/Either";
import { LinkError, LinkResult, UnlinkError, UnlinkResult } from "devlink-core";

import { Modes } from "./mode";
import { ErrorDefinition } from "./errors";

export type CliInputs = {
	cwd: string;
	mode: O.Option<Modes>;
	files: O.Option<string[]>;
};

export type CliConfigs = {
	config: O.Option<string>;
	env: O.Option<string>;
};

export type ActionRequest = {
	cwd: string;
	mode: Modes;
	files: Array<{
		on: string;
		to: string;
		errors: ErrorDefinition[];
	}>;
};

export type ActionResults = LinkResults | UnlinkResults;

export type LinkResults = {
	mode: "link";
	errors: ActionRequest["files"];
	results: OneLinkResult[];
};
export type OneLinkResult = {
	result: E.Either<LinkError, LinkResult>;
	request: ActionRequest["files"][number];
};

export type UnlinkResults = {
	mode: "unlink";
	errors: ActionRequest["files"];
	results: OneUnlinkResult[];
};
export type OneUnlinkResult = {
	result: E.Either<UnlinkError, UnlinkResult>;
	request: ActionRequest["files"][number];
};
