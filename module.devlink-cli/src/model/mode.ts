export type Modes = "link" | "unlink";
export const LINK_MODE = "link";
export const UNLINK_MODE = "unlink";

export function foldMode<T>(handlers: {
	whenLink: () => T;
	whenUnlink: () => T;
	whenUnknown: () => T;
}): (type: Modes | unknown) => T {
	return (type) => {
		switch (type) {
			case LINK_MODE:
				return handlers.whenLink();
			case UNLINK_MODE:
				return handlers.whenUnlink();
			default:
				return handlers.whenUnknown();
		}
	};
}
