import { LINK_MODE, UNLINK_MODE } from "./mode";

export enum ErrorCode {
	MISSING_MODE = "MISSING_MODE",
	MISSING_FILES = "MISSING_FILES",
	INVALID_LINK_FILES_COUNT = "INVALID_LINK_FILES_COUNT",
	INVALID_UNLINK_FILES_COUNT = "INVALID_UNLINK_FILES_COUNT",
	INPURE_EXISTS_CALL_FAILED = "INPURE_EXISTS_CALL_FAILED",
	DEVLINK_PACKAGE_ERROR = "DEVLINK_PACKAGE_ERROR",
	FILE_NOT_EXISTS = "FILE_NOT_EXISTS",
	FILE_IS_SYMLINK_ALREADY = "FILE_IS_SYMLINK_ALREADY",
	FILE_IS_NOT_SYMLINK = "FILE_IS_NOT_SYMLINK",
	FILES_INVALID_TYPE = "FILES_INVALID_TYPE",
}

export type ErrorDefinition = {
	title: string;
	description: string;
	code: ErrorCode;
	tip?: string;
};

export type ErrorReason = {
	component: JSX.Element;
	code: ErrorCode;
	exitCode: number;
};

export const EXIT_CODES: Record<ErrorCode, number> = {
	[ErrorCode.MISSING_MODE]: 1,
	[ErrorCode.MISSING_FILES]: 2,
	[ErrorCode.INVALID_LINK_FILES_COUNT]: 3,
	[ErrorCode.INVALID_UNLINK_FILES_COUNT]: 3,
	[ErrorCode.FILE_NOT_EXISTS]: 4,
	[ErrorCode.FILE_IS_SYMLINK_ALREADY]: 4,
	[ErrorCode.FILE_IS_NOT_SYMLINK]: 4,
	[ErrorCode.FILES_INVALID_TYPE]: 5,
	[ErrorCode.DEVLINK_PACKAGE_ERROR]: 9,
	[ErrorCode.INPURE_EXISTS_CALL_FAILED]: 10,
};

export const ERRORS: Record<ErrorCode, ErrorDefinition> = {
	[ErrorCode.MISSING_MODE]: {
		title: `No mode provided to devlink`,
		description: `Devlink needs to known if you want to create or remove link. Possible values are "${LINK_MODE}" or "${UNLINK_MODE}"`,
		code: ErrorCode.MISSING_MODE,
		tip: `Use command line with defined mode for example: "devlink link /path/to/source.txt /path/to/destination.txt"`,
	},
	[ErrorCode.MISSING_FILES]: {
		title: `No paths to files provided`,
		description: `You need specified file or directory paths that will be used for "${LINK_MODE}" or "${UNLINK_MODE}"`,
		code: ErrorCode.MISSING_FILES,
		tip: `Use command line with defined files for example: "devlink link /path/to/source.txt /path/to/destination.txt"`,
	},
	[ErrorCode.INVALID_LINK_FILES_COUNT]: {
		title: `Invalid count of files provided for "${LINK_MODE}"`,
		description: `Mode ${LINK_MODE}" needs to have event count of files for creating links, every first file is "source" and second is "destination"`,
		code: ErrorCode.INVALID_LINK_FILES_COUNT,
		tip: `Use command line with defined files for example: "devlink link /path/to/source1.txt /path/to/destination1.txt /path/to/source2.txt /path/to/destination2.txt"`,
	},
	[ErrorCode.INVALID_UNLINK_FILES_COUNT]: {
		title: `Invalid count of files provided for "${UNLINK_MODE}"`,
		description: `Mode ${UNLINK_MODE}" needs to have at least one file, that will be used for unlink created link`,
		code: ErrorCode.INVALID_UNLINK_FILES_COUNT,
	},
	[ErrorCode.INPURE_EXISTS_CALL_FAILED]: {
		title: `Error occurred in "fs.existsSync" native function`,
		description: `There is occurred some error that calling native method from nodejs`,
		code: ErrorCode.INPURE_EXISTS_CALL_FAILED,
	},
	[ErrorCode.FILE_NOT_EXISTS]: {
		title: `File not exist on path`,
		description: `Desired file not exists on provided file path`,
		code: ErrorCode.FILE_NOT_EXISTS,
		tip: `Look if file path is right and not contains any spelling errors`,
	},
	[ErrorCode.FILE_IS_SYMLINK_ALREADY]: {
		title: `File is symbolic link already`,
		description: `Desired file is already a symbolic link so is not possible to make link with "${LINK_MODE}"`,
		code: ErrorCode.FILE_IS_SYMLINK_ALREADY,
		tip: `If this link is created by devlink, try to unlink this by "devlink unlink <file-path>"`,
	},
	[ErrorCode.FILE_IS_NOT_SYMLINK]: {
		title: `File is not symbolic link yet`,
		description: `Desired file is not a symbolic link so is not possible to make unlink with "${UNLINK_MODE}"`,
		code: ErrorCode.FILE_IS_NOT_SYMLINK,
		tip: `Use "devlink link <file-path>" to create a symbolic link, that can be unlinked after`,
	},
	[ErrorCode.DEVLINK_PACKAGE_ERROR]: {
		title: "Error occurred in devlink package",
		description:
			"There is some error occurred in devlink npm package, look on original error for more info",
		code: ErrorCode.DEVLINK_PACKAGE_ERROR,
	},
	[ErrorCode.FILES_INVALID_TYPE]: {
		title: `Files are unknown or has different types`,
		description: `Provided files into current mode has different types, or type of one is unknown`,
		code: ErrorCode.FILES_INVALID_TYPE,
	},
};
