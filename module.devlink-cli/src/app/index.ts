import * as F from "fp-ts/function";
import * as E from "fp-ts/Either";

import { ErrorReason, CliInputs, CliConfigs } from "../model";
import { validate } from "../validators";
import { action } from "../actions";
import { main } from "../components";

import { transformErrorCode } from "./exit";

export type AppProps = CliInputs & CliConfigs;

export function app({ cwd, mode, files, env, config }: AppProps) {
	return render(
		F.pipe(
			validate({ cwd, mode, files }),
			E.map(action),
			E.flattenW,
			E.map((results) => main(results, config, env))
		)
	);
}

function render(final: E.Either<ErrorReason, JSX.Element>) {
	return F.pipe(final, E.match(exit, F.identity));
}

function exit(err: ErrorReason) {
	return F.pipe(err, transformErrorCode(process.exitCode), ({ exitCode, component }) => {
		process.exitCode = exitCode;
		return component;
	});
}
