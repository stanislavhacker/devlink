import * as F from "fp-ts/function";
import * as E from "fp-ts/Either";

import { ErrorReason } from "../model";

function errorCode(currentCode: number | undefined, newCode: number) {
	return F.pipe(
		getCurrentCode(currentCode, newCode),
		E.map(useMoreImportantCode(newCode)),
		E.fold(F.identity, F.identity)
	);
}

function getCurrentCode(currentCode: number | undefined, newCode: number): E.Either<never, number> {
	return F.pipe(
		currentCode,
		E.fromPredicate(
			(currentCode) => currentCode !== undefined,
			() => newCode
		),
		E.orElseW((newCode) => E.right(newCode))
	) as E.Either<never, number>;
}

function useMoreImportantCode(newCode: number) {
	return (currentCode: number) => {
		//NOTE: New code has bigger priority
		if (newCode < currentCode) {
			return newCode;
		}
		return currentCode;
	};
}

export function transformErrorCode(currentCode: number | undefined) {
	return ({ component, exitCode, code }: ErrorReason): ErrorReason => ({
		exitCode: errorCode(currentCode, exitCode),
		code,
		component,
	});
}
