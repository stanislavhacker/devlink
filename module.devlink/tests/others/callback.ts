/* eslint-disable @typescript-eslint/no-non-null-assertion */
import "jasmine";
import * as path from "path";
import * as fs from "fs";

import { createStore } from "../store";

//api
import { callback } from "../../src";

describe("callback link", () => {
	const { link } = callback;
	let clean;

	beforeEach(() => {
		clean = createStore();
	});

	it("create link from source.txt => link.txt, ok", (done) => {
		link("/data/sources/source.txt", "/data/links/link.txt", (err, value) => {
			expect(value!.on).toBe(path.normalize("/data/sources/source.txt"));
			expect(value!.to).toBe(path.normalize("/data/links/link.txt"));
			expect(value!.original).toBe(path.normalize("/data/sources/@@linked#source.txt"));
			expect(err).toBe(null);
			done();
		});
	});

	it("create link from sources/directory => links/sources, ok", (done) => {
		link("/data/sources/directory", "/data/links/directory", (err, value) => {
			expect(value!.on).toBe(path.normalize("/data/sources/directory"));
			expect(value!.to).toBe(path.normalize("/data/links/directory"));
			expect(value!.original).toBe(path.normalize("/data/sources/@@linked#directory"));
			expect(err).toBe(null);
			done();
		});
	});

	describe("throw", () => {
		beforeEach(() => {
			(fs.lstatSync as jasmine.Spy).and.returnValue({
				isSymbolicLink: () => true,
				isFile: () => false,
				isDirectory: () => false,
			} as fs.Stats);
			(fs.existsSync as jasmine.Spy).and.throwError("There is some error in existsSync.");
		});

		it("run is throw error", (done) => {
			link("/data/sources/source.txt", "/data/links/link.txt", (err, value) => {
				expect(err!.name).toBe("Call in 'existsSync' failed.");
				expect(err!.description).toBe(
					"There is some error occurred when calling native method from nodejs fs module."
				);
				expect(err!.type).toBe("EXISTS_CALL_FAILED");
				expect(err!.original!.message).toBe("There is some error in existsSync.");
				expect(value).toBe(null);
				done();
			});
		});
	});

	afterEach(() => {
		clean();
	});
});

describe("callback unlink", () => {
	const { link, unlink } = callback;
	let clean;

	beforeEach((done) => {
		clean = createStore();
		link("/data/sources/source.txt", "/data/links/link.txt", () => done());
	});

	it("unlink link from source.txt => link.txt, ok", (done) => {
		unlink("/data/sources/source.txt", (err, value) => {
			expect(value!.on).toBe(path.normalize("/data/sources/source.txt"));
			expect(err).toBe(null);
			done();
		});
	});

	describe("throw", () => {
		beforeEach(() => {
			(fs.lstatSync as jasmine.Spy).and.returnValue({
				isSymbolicLink: () => true,
				isFile: () => false,
				isDirectory: () => false,
			} as fs.Stats);
			(fs.existsSync as jasmine.Spy).and.throwError("There is some error in existsSync.");
		});

		it("run is throw error", (done) => {
			unlink("/data/sources/source.txt", (err, value) => {
				expect(err!.name).toBe("Call in 'existsSync' failed.");
				expect(err!.description).toBe(
					"There is some error occurred when calling native method from nodejs fs module."
				);
				expect(err!.type).toBe("EXISTS_CALL_FAILED");
				expect(err!.original!.message).toBe("There is some error in existsSync.");
				expect(value).toBe(null);
				done();
			});
		});
	});

	afterEach(() => {
		clean();
	});
});

describe("callback linked", () => {
	const { link, linked } = callback;
	let clean;

	beforeEach((done) => {
		clean = createStore();
		link("/data/sources/source.txt", "/data/links/link.txt", () => done());
	});

	it("linked link from source.txt, ok", (done) => {
		linked("/data/sources/source.txt", (value) => {
			expect(value!.on).toBe(path.normalize("/data/sources/source.txt"));
			expect(value!.backup).toBe(path.normalize("/data/sources/@@linked#source.txt"));
			expect(value!.exists).toBe(true);
			done();
		});
	});

	describe("throw", () => {
		beforeEach(() => {
			(fs.lstatSync as jasmine.Spy).and.returnValue({
				isSymbolicLink: () => true,
				isFile: () => false,
				isDirectory: () => false,
			} as fs.Stats);
			(fs.existsSync as jasmine.Spy).and.throwError("There is some error in existsSync.");
		});

		it("run is not never throw error", (done) => {
			linked("/data/sources/source.txt", (value) => {
				expect(value!.on).toBe(path.normalize("/data/sources/source.txt"));
				expect(value!.backup).toBe(path.normalize("/data/sources/@@linked#source.txt"));
				expect(value!.exists).toBe(false);
				done();
			});
		});
	});

	afterEach(() => {
		clean();
	});
});
