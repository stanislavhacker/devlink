/* eslint-disable @typescript-eslint/no-non-null-assertion */
import "jasmine";
import * as path from "path";
import * as fs from "fs";

import { createStore } from "../store";

//api
import { LinkError, UnlinkError, promise } from "../../src";

describe("promise link", () => {
	const { link } = promise;
	let clean;

	beforeEach(() => {
		clean = createStore();
	});

	it("create link from source.txt => link.txt, ok", (done) => {
		link("/data/sources/source.txt", "/data/links/link.txt").then((value) => {
			expect(value!.on).toBe(path.normalize("/data/sources/source.txt"));
			expect(value!.to).toBe(path.normalize("/data/links/link.txt"));
			expect(value!.original).toBe(path.normalize("/data/sources/@@linked#source.txt"));
			done();
		});
	});

	it("create link from sources/directory => links/sources, ok", (done) => {
		link("/data/sources/directory", "/data/links/directory").then((value) => {
			expect(value!.on).toBe(path.normalize("/data/sources/directory"));
			expect(value!.to).toBe(path.normalize("/data/links/directory"));
			expect(value!.original).toBe(path.normalize("/data/sources/@@linked#directory"));
			done();
		});
	});

	describe("throw", () => {
		beforeEach(() => {
			(fs.lstatSync as jasmine.Spy).and.returnValue({
				isSymbolicLink: () => true,
				isFile: () => false,
				isDirectory: () => false,
			} as fs.Stats);
			(fs.existsSync as jasmine.Spy).and.throwError("There is some error in existsSync.");
		});

		it("run is throw error", (done) => {
			link("/data/sources/source.txt", "/data/links/link.txt").catch((err: LinkError) => {
				expect(err!.name).toBe("Call in 'existsSync' failed.");
				expect(err!.description).toBe(
					"There is some error occurred when calling native method from nodejs fs module."
				);
				expect(err!.type).toBe("EXISTS_CALL_FAILED");
				expect(err!.original!.message).toBe("There is some error in existsSync.");
				done();
			});
		});
	});

	afterEach(() => {
		clean();
	});
});

describe("promise unlink", () => {
	const { link, unlink } = promise;
	let clean;

	beforeEach(async () => {
		clean = createStore();
		await link("/data/sources/source.txt", "/data/links/link.txt");
	});

	it("unlink link from source.txt => link.txt, ok", (done) => {
		unlink("/data/sources/source.txt").then((value) => {
			expect(value!.on).toBe(path.normalize("/data/sources/source.txt"));
			done();
		});
	});

	describe("throw", () => {
		beforeEach(() => {
			(fs.lstatSync as jasmine.Spy).and.returnValue({
				isSymbolicLink: () => true,
				isFile: () => false,
				isDirectory: () => false,
			} as fs.Stats);
			(fs.existsSync as jasmine.Spy).and.throwError("There is some error in existsSync.");
		});

		it("run is throw error", (done) => {
			unlink("/data/sources/source.txt").catch((err: UnlinkError) => {
				expect(err!.name).toBe("Call in 'existsSync' failed.");
				expect(err!.description).toBe(
					"There is some error occurred when calling native method from nodejs fs module."
				);
				expect(err!.type).toBe("EXISTS_CALL_FAILED");
				expect(err!.original!.message).toBe("There is some error in existsSync.");
				done();
			});
		});
	});

	afterEach(() => {
		clean();
	});
});

describe("promise linked", () => {
	const { link, linked } = promise;
	let clean;

	beforeEach(async () => {
		clean = createStore();
		await link("/data/sources/source.txt", "/data/links/link.txt");
	});

	it("linked link from source.txt, ok", (done) => {
		linked("/data/sources/source.txt").then((value) => {
			expect(value!.on).toBe(path.normalize("/data/sources/source.txt"));
			expect(value!.backup).toBe(path.normalize("/data/sources/@@linked#source.txt"));
			expect(value!.exists).toBe(true);
			done();
		});
	});

	describe("throw", () => {
		beforeEach(() => {
			(fs.lstatSync as jasmine.Spy).and.returnValue({
				isSymbolicLink: () => true,
				isFile: () => false,
				isDirectory: () => false,
			} as fs.Stats);
			(fs.existsSync as jasmine.Spy).and.throwError("There is some error in existsSync.");
		});

		it("run is never throw error", (done) => {
			linked("/data/sources/source.txt").then((value) => {
				expect(value!.on).toBe(path.normalize("/data/sources/source.txt"));
				expect(value!.backup).toBe(path.normalize("/data/sources/@@linked#source.txt"));
				expect(value!.exists).toBe(false);
				done();
			});
		});
	});

	afterEach(() => {
		clean();
	});
});
