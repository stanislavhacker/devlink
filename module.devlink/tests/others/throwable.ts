/* eslint-disable @typescript-eslint/no-non-null-assertion */
import "jasmine";
import * as path from "path";
import * as fs from "fs";

import { createStore } from "../store";

//api
import { throwable } from "../../src";

describe("throwable link", () => {
	const { link } = throwable;
	let clean;

	beforeEach(() => {
		clean = createStore();
	});

	it("create link from source.txt => link.txt, ok", () => {
		const value = link("/data/sources/source.txt", "/data/links/link.txt");

		expect(value!.on).toBe(path.normalize("/data/sources/source.txt"));
		expect(value!.to).toBe(path.normalize("/data/links/link.txt"));
		expect(value!.original).toBe(path.normalize("/data/sources/@@linked#source.txt"));
	});

	it("create link from sources/directory => links/sources, ok", () => {
		const value = link("/data/sources/directory", "/data/links/directory");

		expect(value!.on).toBe(path.normalize("/data/sources/directory"));
		expect(value!.to).toBe(path.normalize("/data/links/directory"));
		expect(value!.original).toBe(path.normalize("/data/sources/@@linked#directory"));
	});

	describe("throw", () => {
		beforeEach(() => {
			(fs.lstatSync as jasmine.Spy).and.returnValue({
				isSymbolicLink: () => true,
				isFile: () => false,
				isDirectory: () => false,
			} as fs.Stats);
			(fs.existsSync as jasmine.Spy).and.throwError("There is some error in existsSync.");
		});

		it("run is throw error", () => {
			try {
				link("/data/sources/source.txt", "/data/links/link.txt");
				expect(false).toBeTrue();
			} catch (err) {
				expect(err!.name).toBe("Call in 'existsSync' failed.");
				expect(err!.description).toBe(
					"There is some error occurred when calling native method from nodejs fs module."
				);
				expect(err!.type).toBe("EXISTS_CALL_FAILED");
				expect(err!.original!.message).toBe("There is some error in existsSync.");
			}
		});
	});

	afterEach(() => {
		clean();
	});
});

describe("throwable unlink", () => {
	const { link, unlink } = throwable;
	let clean;

	beforeEach(() => {
		clean = createStore();
		link("/data/sources/source.txt", "/data/links/link.txt");
	});

	it("unlink link from source.txt => link.txt, ok", () => {
		const value = unlink("/data/sources/source.txt");

		expect(value!.on).toBe(path.normalize("/data/sources/source.txt"));
	});

	describe("throw", () => {
		beforeEach(() => {
			(fs.lstatSync as jasmine.Spy).and.returnValue({
				isSymbolicLink: () => true,
				isFile: () => false,
				isDirectory: () => false,
			} as fs.Stats);
			(fs.existsSync as jasmine.Spy).and.throwError("There is some error in existsSync.");
		});

		it("run is throw error", () => {
			try {
				unlink("/data/sources/source.txt");
				expect(false).toBeTrue();
			} catch (err) {
				expect(err!.name).toBe("Call in 'existsSync' failed.");
				expect(err!.description).toBe(
					"There is some error occurred when calling native method from nodejs fs module."
				);
				expect(err!.type).toBe("EXISTS_CALL_FAILED");
				expect(err!.original!.message).toBe("There is some error in existsSync.");
			}
		});
	});

	afterEach(() => {
		clean();
	});
});

describe("throwable linked", () => {
	const { link, linked } = throwable;
	let clean;

	beforeEach(() => {
		clean = createStore();
		link("/data/sources/source.txt", "/data/links/link.txt");
	});

	it("linked link from source.txt, ok", () => {
		const value = linked("/data/sources/source.txt");

		expect(value!.on).toBe(path.normalize("/data/sources/source.txt"));
		expect(value!.backup).toBe(path.normalize("/data/sources/@@linked#source.txt"));
		expect(value!.exists).toBe(true);
	});

	describe("throw", () => {
		beforeEach(() => {
			(fs.lstatSync as jasmine.Spy).and.returnValue({
				isSymbolicLink: () => true,
				isFile: () => false,
				isDirectory: () => false,
			} as fs.Stats);
			(fs.existsSync as jasmine.Spy).and.throwError("There is some error in existsSync.");
		});

		it("run is never throw error", () => {
			try {
				const value = linked("/data/sources/source.txt");
				expect(value!.on).toBe(path.normalize("/data/sources/source.txt"));
				expect(value!.backup).toBe(path.normalize("/data/sources/@@linked#source.txt"));
				expect(value!.exists).toBe(false);
			} catch (err) {
				expect(false).toBeTrue();
			}
		});
	});

	afterEach(() => {
		clean();
	});
});
