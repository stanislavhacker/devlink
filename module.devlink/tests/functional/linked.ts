/* eslint-disable @typescript-eslint/no-non-null-assertion */
import "jasmine";
import * as E from "fp-ts/Either";
import * as path from "path";
import * as fs from "fs";

import { createStore } from "../store";
import { toValue } from "../../src/utils";

//api
import { functional } from "../../src";

describe("functional link", () => {
	const { link, linked } = functional;
	let clean;

	beforeEach(async () => {
		clean = createStore();
	});

	describe("without error", () => {
		it("check link on source.txt is false, ok", async () => {
			const result = linked("/data/sources/source.txt");
			expect(E.isRight(result)).toBeTrue();
			const value = toValue(result);
			expect(value!.on).toBe(path.normalize("/data/sources/source.txt"));
			expect(value!.backup).toBe(path.normalize("/data/sources/@@linked#source.txt"));
			expect(value!.exists).toBe(false);
		});

		it("check link on source.txt is true, ok", async () => {
			link("/data/sources/source.txt", "/data/links/link.txt");

			const result = linked("/data/sources/source.txt");
			expect(E.isRight(result)).toBeTrue();
			const value = toValue(result);
			expect(value!.on).toBe(path.normalize("/data/sources/source.txt"));
			expect(value!.backup).toBe(path.normalize("/data/sources/@@linked#source.txt"));
			expect(value!.exists).toBe(true);
		});

		it("check link on link.txt is false, ok", async () => {
			link("/data/sources/source.txt", "/data/links/link.txt");

			const result = linked("/data/links/link.txt");
			expect(E.isRight(result)).toBeTrue();
			const value = toValue(result);
			expect(value!.on).toBe(path.normalize("/data/links/link.txt"));
			expect(value!.backup).toBe(path.normalize("/data/links/@@linked#link.txt"));
			expect(value!.exists).toBe(false);
		});
	});

	describe("with error", () => {
		describe("throw in 'lstatSync' function", () => {
			beforeEach(() => {
				(fs.lstatSync as jasmine.Spy).and.throwError("There is some error in lstatSync.");
			});

			it("run", () => {
				const result = linked("/data/sources/source.txt");
				expect(E.isRight(result)).toBeTrue();

				const value = toValue(result);
				expect(value!.on).toBe(path.normalize("/data/sources/source.txt"));
				expect(value!.backup).toBe(path.normalize("/data/sources/@@linked#source.txt"));
				expect(value!.exists).toBe(false);
			});
		});

		describe("throw in 'existsSync' function", () => {
			beforeEach(() => {
				(fs.lstatSync as jasmine.Spy).and.returnValue({
					isSymbolicLink: () => true,
					isFile: () => false,
					isDirectory: () => false,
				} as fs.Stats);
				(fs.existsSync as jasmine.Spy).and.throwError("There is some error in existsSync.");
			});

			it("run", () => {
				const result = linked("/data/sources/source.txt");
				expect(E.isRight(result)).toBeTrue();

				const value = toValue(result);
				expect(value!.on).toBe(path.normalize("/data/sources/source.txt"));
				expect(value!.backup).toBe(path.normalize("/data/sources/@@linked#source.txt"));
				expect(value!.exists).toBe(false);
			});
		});
	});

	afterEach(() => {
		clean();
	});
});
