/* eslint-disable @typescript-eslint/no-non-null-assertion */
import "jasmine";
import * as E from "fp-ts/Either";
import * as path from "path";
import * as fs from "fs";

import { createStore } from "../store";
import { toValue, toError } from "../../src/utils";

//api
import { functional } from "../../src";

describe("functional unlink", () => {
	const { link, unlink } = functional;
	let clean;

	beforeEach(() => {
		clean = createStore();
	});

	describe("without error", () => {
		it("remove link from source.txt => link.txt, ok", async () => {
			link("/data/sources/source.txt", "/data/links/link.txt");

			const result = unlink("/data/sources/source.txt");
			expect(E.isRight(result)).toBeTrue();
			const value = toValue(result);
			expect(value!.on).toBe(path.normalize("/data/sources/source.txt"));

			expect(fs.existsSync("/data/sources/source.txt")).toBe(true);
			expect(fs.existsSync("/data/links/link.txt")).toBe(true);
			expect(fs.existsSync("/data/sources/@@linked#source.txt")).toBe(false);

			expect(fs.readFileSync("/data/sources/source.txt").toString()).toBe(
				"This is original file."
			);
			expect(fs.readFileSync("/data/links/link.txt").toString()).toBe(
				"This is linked file with linked content."
			);
		});

		it("remove link from sources/directory => links/sources, ok", async () => {
			link("/data/sources/directory", "/data/links/directory");

			const result = unlink("/data/sources/directory");
			expect(E.isRight(result)).toBeTrue();
			const value = toValue(result);
			expect(value!.on).toBe(path.normalize("/data/sources/directory"));

			expect(fs.existsSync("/data/sources/directory")).toBe(true);
			expect(fs.existsSync("/data/links/directory")).toBe(true);
			expect(fs.existsSync("/data/sources/@@linked#directory")).toBe(false);

			expect(fs.readFileSync("/data/sources/directory/file.txt").toString()).toBe(
				"This is original file in directory."
			);
			expect(fs.readFileSync("/data/links/directory/file.txt").toString()).toBe(
				"This is linked file with linked content in directory."
			);
		});
	});

	describe("with error", () => {
		describe("unknown file type", () => {
			beforeEach(() => {
				(fs.lstatSync as jasmine.Spy).and.returnValue({
					isSymbolicLink: () => false,
					isFile: () => false,
					isDirectory: () => false,
				} as fs.Stats);
			});

			it("run", () => {
				const result = unlink("/data/sources/source.txt");
				expect(E.isLeft(result)).toBeTrue();

				const err = toError(result);
				expect(err!.name).toBe("No symlink exists on path.");
				expect(err!.description).toMatch(
					/On provided path ".*" there is no symlink to unlink\./g
				);
				expect(err!.type).toBe("NO_SYMLINK_ON_PATH");
				expect(err!.original).toBeUndefined();
			});
		});

		describe("different link already created", () => {
			beforeEach(() => {
				(fs.lstatSync as jasmine.Spy).and.returnValue({
					isSymbolicLink: () => true,
					isFile: () => false,
					isDirectory: () => false,
				} as fs.Stats);
			});

			it("run", () => {
				const result = unlink("/data/sources/source.txt");
				expect(E.isLeft(result)).toBeTrue();

				const err = toError(result);
				expect(err!.name).toBe("No symlink exists on path.");
				expect(err!.description).toMatch(
					/On provided path ".*" there is no symlink to unlink\./g
				);
				expect(err!.type).toBe("NO_SYMLINK_ON_PATH");
				expect(err!.original).toBeUndefined();
			});
		});

		describe("throw in 'existsSync' function", () => {
			beforeEach(() => {
				(fs.lstatSync as jasmine.Spy).and.returnValue({
					isSymbolicLink: () => true,
					isFile: () => false,
					isDirectory: () => false,
				} as fs.Stats);
				(fs.existsSync as jasmine.Spy).and.throwError("There is some error in existsSync.");
			});

			it("run", () => {
				const result = unlink("/data/sources/source.txt");
				expect(E.isLeft(result)).toBeTrue();

				const err = toError(result);
				expect(err!.name).toBe("Call in 'existsSync' failed.");
				expect(err!.description).toBe(
					"There is some error occurred when calling native method from nodejs fs module."
				);
				expect(err!.type).toBe("EXISTS_CALL_FAILED");
				expect(err!.original!.message).toBe("There is some error in existsSync.");
			});
		});

		describe("throw in 'lstatSync' function", () => {
			beforeEach(() => {
				(fs.lstatSync as jasmine.Spy).and.throwError("There is some error in lstatSync.");
			});

			it("run", () => {
				const result = unlink("/data/sources/source.txt");
				expect(E.isLeft(result)).toBeTrue();

				const err = toError(result);
				expect(err!.name).toBe("Call in 'lstatSync' failed.");
				expect(err!.description).toBe(
					"There is some error occurred when calling native method from nodejs fs module."
				);
				expect(err!.type).toBe("TYPE_FILE_ERROR");
				expect(err!.original!.message).toBe("There is some error in lstatSync.");
			});
		});

		describe("throw in 'renameSync' function", () => {
			beforeEach(() => {
				(fs.renameSync as jasmine.Spy).and.throwError("There is some error in renameSync.");
			});

			it("run", () => {
				const result = unlink("/data/sources/source.txt");
				expect(E.isLeft(result)).toBeTrue();

				const err = toError(result);
				expect(err!.name).toBe("No symlink exists on path.");
				expect(err!.description).toMatch(
					/On provided path ".*" there is no symlink to unlink\./g
				);
				expect(err!.type).toBe("NO_SYMLINK_ON_PATH");
				expect(err!.original).toBeUndefined();
			});
		});

		describe("throw in 'symlinkSync' function", () => {
			beforeEach(() => {
				(fs.symlinkSync as jasmine.Spy).and.throwError(
					"There is some error in symlinkSync."
				);
			});

			it("run and revert back", () => {
				const result = unlink("/data/sources/source.txt");
				expect(E.isLeft(result)).toBeTrue();

				const err = toError(result);
				expect(err!.name).toBe("No symlink exists on path.");
				expect(err!.description).toMatch(
					/On provided path ".*" there is no symlink to unlink\./g
				);
				expect(err!.type).toBe("NO_SYMLINK_ON_PATH");
				expect(err!.original).toBeUndefined();
			});
		});
	});

	afterEach(() => {
		clean();
	});
});
