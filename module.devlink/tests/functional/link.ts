/* eslint-disable @typescript-eslint/no-non-null-assertion */
import "jasmine";
import * as E from "fp-ts/Either";
import * as path from "path";
import * as fs from "fs";

import { createStore } from "../store";
import { toValue, toError } from "../../src/utils";

//api
import { functional } from "../../src";

describe("functional link", () => {
	const { link } = functional;
	let clean;

	beforeEach(async () => {
		clean = createStore();
	});

	describe("without error", () => {
		it("create link from source.txt => link.txt, ok", async () => {
			const result = link("/data/sources/source.txt", "/data/links/link.txt");
			expect(E.isRight(result)).toBeTrue();
			const value = toValue(result);
			expect(value!.on).toBe(path.normalize("/data/sources/source.txt"));
			expect(value!.to).toBe(path.normalize("/data/links/link.txt"));
			expect(value!.original).toBe(path.normalize("/data/sources/@@linked#source.txt"));

			expect(fs.existsSync("/data/sources/source.txt")).toBe(true);
			expect(fs.existsSync("/data/links/link.txt")).toBe(true);
			expect(fs.existsSync("/data/sources/@@linked#source.txt")).toBe(true);

			expect(fs.readFileSync("/data/sources/source.txt").toString()).toBe(
				"This is linked file with linked content."
			);
			expect(fs.readFileSync("/data/links/link.txt").toString()).toBe(
				"This is linked file with linked content."
			);
			expect(fs.readFileSync("/data/sources/@@linked#source.txt").toString()).toBe(
				"This is original file."
			);
		});

		it("create link from sources/directory => links/sources, ok", async () => {
			const result = link("/data/sources/directory", "/data/links/directory");
			expect(E.isRight(result)).toBeTrue();
			const value = toValue(result);
			expect(value!.on).toBe(path.normalize("/data/sources/directory"));
			expect(value!.to).toBe(path.normalize("/data/links/directory"));
			expect(value!.original).toBe(path.normalize("/data/sources/@@linked#directory"));

			expect(fs.existsSync("/data/sources/directory")).toBe(true);
			expect(fs.existsSync("/data/links/directory")).toBe(true);
			expect(fs.existsSync("/data/sources/@@linked#directory")).toBe(true);

			expect(fs.readFileSync("/data/sources/directory/file.txt").toString()).toBe(
				"This is linked file with linked content in directory."
			);
			expect(fs.readFileSync("/data/links/directory/file.txt").toString()).toBe(
				"This is linked file with linked content in directory."
			);
			expect(fs.readFileSync("/data/sources/@@linked#directory/file.txt").toString()).toBe(
				"This is original file in directory."
			);
		});

		it("create link from source.txt => link.txt, already exists", async () => {
			const one = link("/data/sources/source.txt", "/data/links/link.txt");
			expect(E.isRight(one)).toBeTrue();

			const result = link("/data/sources/source.txt", "/data/links/link.txt");
			expect(E.isRight(result)).toBeTrue();
			const value = toValue(result);
			expect(value!.on).toBe(path.normalize("/data/sources/source.txt"));
			expect(value!.to).toBe(path.normalize("/data/links/link.txt"));
			expect(value!.original).toBe(path.normalize("/data/sources/@@linked#source.txt"));

			expect(fs.existsSync("/data/sources/source.txt")).toBe(true);
			expect(fs.existsSync("/data/links/link.txt")).toBe(true);
			expect(fs.existsSync("/data/sources/@@linked#source.txt")).toBe(true);

			expect(fs.readFileSync("/data/sources/source.txt").toString()).toBe(
				"This is linked file with linked content."
			);
			expect(fs.readFileSync("/data/links/link.txt").toString()).toBe(
				"This is linked file with linked content."
			);
			expect(fs.readFileSync("/data/sources/@@linked#source.txt").toString()).toBe(
				"This is original file."
			);
		});

		it("create link from sources/directory => links/sources, already exists", async () => {
			const one = link("/data/sources/directory", "/data/links/directory");
			expect(E.isRight(one)).toBeTrue();

			const result = link("/data/sources/directory", "/data/links/directory");
			expect(E.isRight(result)).toBeTrue();
			const value = toValue(result);
			expect(value!.on).toBe(path.normalize("/data/sources/directory"));
			expect(value!.to).toBe(path.normalize("/data/links/directory"));
			expect(value!.original).toBe(path.normalize("/data/sources/@@linked#directory"));

			expect(fs.existsSync("/data/sources/directory")).toBe(true);
			expect(fs.existsSync("/data/links/directory")).toBe(true);
			expect(fs.existsSync("/data/sources/@@linked#directory")).toBe(true);

			expect(fs.readFileSync("/data/sources/directory/file.txt").toString()).toBe(
				"This is linked file with linked content in directory."
			);
			expect(fs.readFileSync("/data/links/directory/file.txt").toString()).toBe(
				"This is linked file with linked content in directory."
			);
			expect(fs.readFileSync("/data/sources/@@linked#directory/file.txt").toString()).toBe(
				"This is original file in directory."
			);
		});
	});

	describe("with error", () => {
		describe("unknown file type", () => {
			beforeEach(() => {
				(fs.lstatSync as jasmine.Spy).and.returnValue({
					isSymbolicLink: () => false,
					isFile: () => false,
					isDirectory: () => false,
				} as fs.Stats);
			});

			it("run", () => {
				const result = link("/data/sources/source.txt", "/data/links/link.txt");
				expect(E.isLeft(result)).toBeTrue();

				const err = toError(result);
				expect(err!.name).toBe("Unknown file for creating link.");
				expect(err!.description).toMatch(
					/Unknown file type on ".*"\. Needs to by file or directory\./g
				);
				expect(err!.type).toBe("UNKNOWN_FILE_TYPE");
				expect(err!.original).toBeUndefined();
			});
		});

		describe("different link already created", () => {
			beforeEach(() => {
				(fs.lstatSync as jasmine.Spy).and.returnValue({
					isSymbolicLink: () => true,
					isFile: () => false,
					isDirectory: () => false,
				} as fs.Stats);
			});

			it("run", () => {
				const result = link("/data/sources/source.txt", "/data/links/link.txt");
				expect(E.isLeft(result)).toBeTrue();

				const err = toError(result);
				expect(err!.name).toBe("Symlink already exists.");
				expect(err!.description).toMatch(
					/Symlink on ".*" already exists and can not be created\./g
				);
				expect(err!.type).toBe("SYMLINK_EXISTS_ALREADY");
				expect(err!.original).toBeUndefined();
			});
		});

		describe("throw in 'existsSync' function", () => {
			beforeEach(() => {
				(fs.lstatSync as jasmine.Spy).and.returnValue({
					isSymbolicLink: () => true,
					isFile: () => false,
					isDirectory: () => false,
				} as fs.Stats);
				(fs.existsSync as jasmine.Spy).and.throwError("There is some error in existsSync.");
			});

			it("run", () => {
				const result = link("/data/sources/source.txt", "/data/links/link.txt");
				expect(E.isLeft(result)).toBeTrue();

				const err = toError(result);
				expect(err!.name).toBe("Call in 'existsSync' failed.");
				expect(err!.description).toBe(
					"There is some error occurred when calling native method from nodejs fs module."
				);
				expect(err!.type).toBe("EXISTS_CALL_FAILED");
				expect(err!.original!.message).toBe("There is some error in existsSync.");
			});
		});

		describe("throw in 'lstatSync' function", () => {
			beforeEach(() => {
				(fs.lstatSync as jasmine.Spy).and.throwError("There is some error in lstatSync.");
			});

			it("run", () => {
				const result = link("/data/sources/source.txt", "/data/links/link.txt");
				expect(E.isLeft(result)).toBeTrue();

				const err = toError(result);
				expect(err!.name).toBe("Call in 'lstatSync' failed.");
				expect(err!.description).toBe(
					"There is some error occurred when calling native method from nodejs fs module."
				);
				expect(err!.type).toBe("TYPE_FILE_ERROR");
				expect(err!.original!.message).toBe("There is some error in lstatSync.");
			});
		});

		describe("throw in 'renameSync' function", () => {
			beforeEach(() => {
				(fs.renameSync as jasmine.Spy).and.throwError("There is some error in renameSync.");
			});

			it("run", () => {
				const result = link("/data/sources/source.txt", "/data/links/link.txt");
				expect(E.isLeft(result)).toBeTrue();

				const err = toError(result);
				expect(err!.name).toBe("Call in 'renameSync' failed.");
				expect(err!.description).toBe(
					"There is some error occurred when calling native method from nodejs fs module."
				);
				expect(err!.type).toBe("RENAME_FILE_ERROR");
				expect(err!.original!.message).toBe("There is some error in renameSync.");
			});
		});

		describe("throw in 'symlinkSync' function", () => {
			beforeEach(() => {
				(fs.symlinkSync as jasmine.Spy).and.throwError(
					"There is some error in symlinkSync."
				);
			});

			it("run and revert back", () => {
				const result = link("/data/sources/source.txt", "/data/links/link.txt");
				expect(E.isLeft(result)).toBeTrue();

				const err = toError(result);
				expect(err!.name).toBe("Call in 'symlinkSync' failed.");
				expect(err!.description).toBe(
					"There is some error occurred when calling native method from nodejs fs module."
				);
				expect(err!.type).toBe("SYMLINK_FILE_ERROR");
				expect(err!.original!.message).toBe("There is some error in symlinkSync.");

				expect(fs.readFileSync("/data/sources/source.txt").toString()).toBe(
					"This is original file."
				);
				expect(fs.readFileSync("/data/links/link.txt").toString()).toBe(
					"This is linked file with linked content."
				);
				expect(fs.existsSync("/data/sources/@@linked#source.txt")).toBe(false);
			});
		});
	});

	afterEach(() => {
		clean();
	});
});
