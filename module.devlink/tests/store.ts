import { mockFs } from "./mock-fs";

export function createStore(): () => void {
	return mockFs({
		"/data": {
			// /data/links
			links: {
				directory: {
					"file.txt": "This is linked file with linked content in directory.",
				},
				"link.txt": "This is linked file with linked content.",
			},
			// /data/links
			sources: {
				directory: {
					"file.txt": "This is original file in directory.",
				},
				"source.txt": "This is original file.",
			},
		},
	});
}
