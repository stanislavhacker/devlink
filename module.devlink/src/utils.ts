import { LinkError, UnlinkError } from "./link";
import * as E from "fp-ts/Either";
import * as F from "fp-ts/function";

export interface LinkThrow extends Error {
	type: LinkError["type"] | UnlinkError["type"];
	description: string;
	original?: Error;
}

export function createError(orig: LinkError | UnlinkError | null) {
	if (!orig) {
		return null;
	}

	const err = new Error() as LinkThrow;
	err.name = orig.name;
	err.message = orig.description;
	err.description = orig.description;
	err.type = orig.type;
	err.original = orig.original;
	return err;
}

export function toValue<R, A>(result: E.Either<R, A>) {
	return F.pipe(
		result,
		E.fold(
			() => null,
			(res) => res
		)
	);
}
export function toError<R, A>(result: E.Either<R, A>) {
	return F.pipe(
		result,
		E.swap,
		E.fold(
			() => null,
			(res) => res
		)
	);
}
