import * as F from "fp-ts/function";
import * as E from "fp-ts/Either";
import * as fs from "fs";

import { Err, error } from "../errors";

export type Errors = "SYMLINK_FILE_ERROR";

export function symlink(
	from: string,
	to: string,
	type?: "file" | "dir"
): E.Either<Err<Errors>, { from: string; to: string }> {
	return F.pipe(
		E.tryCatch(
			() => symlinkSync(from, to, type),
			(e: Error) =>
				error<Errors>(
					"SYMLINK_FILE_ERROR",
					"Call in 'symlinkSync' failed.",
					"There is some error occurred when calling native method from nodejs fs module.",
					e
				)
		)
	);
}

function symlinkSync(from: string, to: string, type?: "file" | "dir") {
	fs.symlinkSync(from, to, type);
	return { from, to };
}
