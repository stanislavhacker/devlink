import * as F from "fp-ts/function";
import * as E from "fp-ts/Either";
import * as fs from "fs";

import { Err, error } from "../errors";

export type Errors = "EXISTS_CALL_FAILED";

export function exists(on: string): E.Either<Err<Errors>, boolean> {
	return F.pipe(
		E.tryCatch(
			() => existsSync(on),
			(e: Error) =>
				error<Errors>(
					"EXISTS_CALL_FAILED",
					"Call in 'existsSync' failed.",
					"There is some error occurred when calling native method from nodejs fs module.",
					e
				)
		)
	);
}

function existsSync(on: string): boolean {
	return fs.existsSync(on);
}
