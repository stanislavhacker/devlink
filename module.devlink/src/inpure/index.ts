import { exists, Errors as ExistsErrors } from "./exists";
import { rename, Errors as RenameErrors } from "./rename";
import { symlink, Errors as SymlinkErrors } from "./symlink";
import { unlink, Errors as UnlinkErrors } from "./unlink";
import { fileType, Errors as FileTypeErrors } from "./type";

export type { ExistsErrors, RenameErrors, SymlinkErrors, UnlinkErrors, FileTypeErrors };

export { exists, rename, symlink, unlink, fileType };
