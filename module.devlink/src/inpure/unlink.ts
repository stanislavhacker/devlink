import * as F from "fp-ts/function";
import * as E from "fp-ts/Either";
import * as fs from "fs";

import { Err, error } from "../errors";

export type Errors = "UNLINK_FILE_ERROR";

export function unlink(on: string): E.Either<Err<Errors>, string> {
	return F.pipe(
		E.tryCatch(
			() => unlinkSync(on),
			(e: Error) =>
				error<Errors>(
					"UNLINK_FILE_ERROR",
					"Call in 'unlinkSync' failed.",
					"There is some error occurred when calling native method from nodejs fs module.",
					e
				)
		)
	);
}

function unlinkSync(on: string) {
	fs.unlinkSync(on);
	return on;
}
