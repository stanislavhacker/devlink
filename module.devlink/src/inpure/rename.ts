import * as F from "fp-ts/function";
import * as E from "fp-ts/Either";
import * as fs from "fs";

import { Err, error } from "../errors";

export type Errors = "RENAME_FILE_ERROR";

export function rename(
	from: string,
	to: string
): E.Either<Err<Errors>, { from: string; to: string }> {
	return F.pipe(
		E.tryCatch(
			() => renameSync(from, to),
			(e: Error) =>
				error<Errors>(
					"RENAME_FILE_ERROR",
					"Call in 'renameSync' failed.",
					"There is some error occurred when calling native method from nodejs fs module.",
					e
				)
		)
	);
}

function renameSync(from: string, to: string) {
	fs.renameSync(from, to);
	return { from, to };
}
