import * as F from "fp-ts/function";
import * as E from "fp-ts/Either";
import * as fs from "fs";

import { Err, error } from "../errors";

export type Errors = "TYPE_FILE_ERROR";

export enum FileType {
	Unknown,
	Link,
	File,
	Directory,
}

export function fileType(on: string): E.Either<Err<Errors>, FileType> {
	return F.pipe(
		E.tryCatch(
			() => fileTypeSync(on),
			(e: Error) =>
				error<Errors>(
					"TYPE_FILE_ERROR",
					"Call in 'lstatSync' failed.",
					"There is some error occurred when calling native method from nodejs fs module.",
					e
				)
		)
	);
}

function fileTypeSync(on: string) {
	const stat = fs.lstatSync(on);

	if (stat.isSymbolicLink()) {
		return FileType.Link;
	}
	if (stat.isFile()) {
		return FileType.File;
	}
	if (stat.isDirectory()) {
		return FileType.Directory;
	}
	return FileType.Unknown;
}
