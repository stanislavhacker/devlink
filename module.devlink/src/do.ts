import * as F from "fp-ts/function";
import * as E from "fp-ts/Either";

import {
	rename,
	symlink,
	exists,
	unlink,
	RenameErrors,
	SymlinkErrors,
	ExistsErrors,
	UnlinkErrors,
	FileTypeErrors,
} from "./inpure";
import { error } from "./errors";

export type { SymlinkErrors, RenameErrors, ExistsErrors, UnlinkErrors, FileTypeErrors };

export function doRenameToBackup(on: string) {
	return (link: string) =>
		F.pipe(
			rename(on, link),
			E.map((res) => res)
		);
}

export function doRenameFromBackup(on: string) {
	return (link: string) =>
		F.pipe(
			rename(link, on),
			E.map((res) => res)
		);
}

export function doSymlinkOrRevert(link: string, type: "dir" | "file") {
	return ({ to, from }: { to: string; from: string }) =>
		F.pipe(
			symlink(link, from, type),
			E.orElseW((e) =>
				F.pipe(
					rename(to, from),
					E.map(() => E.left(e)),
					E.flattenW
				)
			),
			E.map((res) => ({ ...res, original: to }))
		);
}

export function doExists() {
	return (link: string) =>
		F.pipe(
			exists(link),
			E.map((exists) => ({ exists, link }))
		);
}

export function doUnlinkOrError(on: string) {
	return ({ exists, link }: { exists: boolean; link: string }) =>
		F.pipe(
			exists,
			E.fromPredicate(
				(state) => state,
				() =>
					error<NoSymlinkTypeErrors>(
						"NO_SYMLINK_ON_PATH",
						"No symlink exists on path.",
						`On provided path "${on}" there is no symlink to unlink.`
					)
			),
			E.chainW(() => unlink(on)),
			E.map(() => link)
		);
}

export type SymlinkExistsErrors = "SYMLINK_EXISTS_ALREADY";

export function doExistingOrError(on: string) {
	return ({ exists, link }: { exists: boolean; link: string }) =>
		F.pipe(
			exists,
			E.fromPredicate(
				(state) => state,
				() =>
					error<SymlinkExistsErrors>(
						"SYMLINK_EXISTS_ALREADY",
						"Symlink already exists.",
						`Symlink on "${on}" already exists and can not be created.`
					)
			),
			E.map(() => ({ exists, original: link }))
		);
}

export type UnknownFileTypeErrors = "UNKNOWN_FILE_TYPE";

export function doUnknown(on: string) {
	return F.pipe(
		E.left(
			error<UnknownFileTypeErrors>(
				"UNKNOWN_FILE_TYPE",
				"Unknown file for creating link.",
				`Unknown file type on "${on}". Needs to by file or directory.`
			)
		)
	);
}

export type NoSymlinkTypeErrors = "NO_SYMLINK_ON_PATH";

export function doNoSymlink(on: string) {
	return F.pipe(
		E.left(
			error<NoSymlinkTypeErrors>(
				"NO_SYMLINK_ON_PATH",
				"No symlink exists on path.",
				`On provided path "${on}" there is no symlink to unlink.`
			)
		)
	);
}
