export type Err<T> = {
	type: T;
	original?: Error;
	name: string;
	description: string;
};

export function error<T>(type: T, name: string, description: string, original?: Error): Err<T> {
	return { type, name, description, original };
}
