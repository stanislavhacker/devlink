import * as F from "fp-ts/function";
import * as E from "fp-ts/Either";

import { fileType, FileTypeErrors } from "./inpure";
import { Err } from "./errors/index";

export enum FileType {
	Unknown,
	Link,
	File,
	Directory,
}

export function type(on: string): E.Either<Err<FileTypeErrors>, FileType> {
	return F.pipe(fileType(on));
}

export function foldType<T>(handlers: {
	whenUnknown: () => T;
	whenLink: () => T;
	whenFile: () => T;
	whenDirectory: () => T;
}): (type: FileType) => T {
	return (type) => {
		switch (type) {
			case FileType.Unknown:
				return handlers.whenUnknown();
			case FileType.Link:
				return handlers.whenLink();
			case FileType.File:
				return handlers.whenFile();
			case FileType.Directory:
				return handlers.whenDirectory();
		}
	};
}
