import * as F from "fp-ts/function";
import * as E from "fp-ts/Either";
import * as A from "fp-ts/Array";

import * as path from "path";

export function normalize(paths: Array<string>): E.Either<never, Array<string>> {
	return F.pipe(
		paths,
		A.map((p) => path.normalize(p)),
		E.right
	);
}

export function linkName(on: string): E.Either<never, string> {
	return F.pipe(
		on,
		(on) => path.join(path.dirname(on), `@@linked#${path.basename(on)}`),
		E.right
	);
}
