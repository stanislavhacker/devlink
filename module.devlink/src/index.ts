import { createError, LinkThrow, toError, toValue } from "./utils";
import {
	link,
	LinkError,
	LinkResult,
	unlink,
	UnlinkError,
	UnlinkResult,
	linked,
	LinkedResult,
} from "./link";

export type { LinkResult, LinkError, UnlinkResult, UnlinkError, LinkedResult, LinkThrow };

//FUNCTIONAL

export const functional = {
	link,
	unlink,
	linked,
};

//PROMISE

export const promise = {
	link: (on: string, to: string): Promise<LinkResult> =>
		new Promise((resolve, reject) => {
			const results = link(on, to);
			const value = toValue(results);

			if (value) {
				resolve(value);
			} else {
				reject(toError(results));
			}
		}),
	unlink: (on: string): Promise<UnlinkResult> =>
		new Promise((resolve, reject) => {
			const results = unlink(on);
			const value = toValue(results);

			if (value) {
				resolve(value);
			} else {
				reject(toError(results));
			}
		}),
	linked: (on: string): Promise<LinkedResult | null> =>
		new Promise((resolve) => {
			const results = linked(on);
			const value = toValue(results);

			resolve(value);
		}),
};

//CALLBACK

export const callback = {
	link: (
		on: string,
		to: string,
		onDone: (err: LinkThrow | null, res: LinkResult | null) => void
	) => {
		const results = link(on, to);
		onDone(createError(toError(results)), toValue(results));
	},
	unlink: (on: string, onDone: (err: LinkThrow | null, res: UnlinkResult | null) => void) => {
		const results = unlink(on);
		onDone(createError(toError(results)), toValue(results));
	},
	linked: (on: string, onDone: (res: LinkedResult | null) => void) => {
		const results = linked(on);
		onDone(toValue(results));
	},
};

//THROWABLE

export const throwable = {
	link: (on: string, to: string): LinkResult => {
		const results = link(on, to);
		const value = toValue(results);

		if (value) {
			return value;
		}
		throw createError(toError(results));
	},
	unlink: (on: string): UnlinkResult => {
		const results = unlink(on);
		const value = toValue(results);

		if (value) {
			return value;
		}
		throw createError(toError(results));
	},
	linked: (on: string): LinkedResult | null => {
		const results = linked(on);
		return toValue(results);
	},
};
