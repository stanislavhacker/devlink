import * as F from "fp-ts/function";
import * as E from "fp-ts/Either";

import { Err } from "./errors";
import { foldType, type } from "./type";
import { linkName, normalize } from "./path";
import {
	doExistingOrError,
	doExists,
	doNoSymlink,
	doRenameToBackup,
	doRenameFromBackup,
	doSymlinkOrRevert,
	doUnknown,
	doUnlinkOrError,
	UnlinkErrors,
	RenameErrors,
	SymlinkErrors,
	SymlinkExistsErrors,
	UnknownFileTypeErrors,
	NoSymlinkTypeErrors,
	FileTypeErrors,
	ExistsErrors,
} from "./do";

//link

export type LinkResult = {
	on: string;
	to: string;
	original: string;
};

export type LinkError = Err<
	| RenameErrors
	| SymlinkErrors
	| SymlinkExistsErrors
	| UnknownFileTypeErrors
	| FileTypeErrors
	| ExistsErrors
>;

export function link(on: string, to: string): E.Either<LinkError, LinkResult> {
	return F.pipe(
		normalize([on, to]),
		E.chain(([on, to]) =>
			F.pipe(
				type(on),
				E.map(
					foldType({
						whenFile: () => handleFile(on, to),
						whenDirectory: () => handleDirectory(on, to),
						whenLink: () => handleLink(on, to),
						//err
						whenUnknown: () => handleUnknown(on),
					})
				),
				E.flattenW
			)
		)
	);
}

//unlink

export type UnlinkResult = {
	on: string;
};

export type UnlinkError = Err<
	NoSymlinkTypeErrors | UnlinkErrors | FileTypeErrors | ExistsErrors | RenameErrors
>;

export function unlink(on: string): E.Either<UnlinkError, UnlinkResult> {
	return F.pipe(
		normalize([on]),
		E.chain(([on]) =>
			F.pipe(
				type(on),
				E.map(
					foldType({
						whenLink: () => handleUnlink(on),
						//err
						whenFile: () => handleNoSymlink(on),
						whenDirectory: () => handleNoSymlink(on),
						whenUnknown: () => handleNoSymlink(on),
					})
				),
				E.flattenW
			)
		)
	);
}

//linked

export type LinkedResult = {
	on: string;
	backup: string;
	exists: boolean;
};

export function linked(on: string): E.Either<never, LinkedResult> {
	return F.pipe(
		normalize([on]),
		E.chain(([on]) =>
			F.pipe(
				type(on),
				E.map(
					foldType({
						whenLink: () => handleLinked(on),
						whenFile: () => handleNotLinked(on),
						whenDirectory: () => handleNotLinked(on),
						whenUnknown: () => handleNotLinked(on),
					})
				),
				E.flattenW,
				E.orElseW(() => handleNotLinked(on))
			)
		)
	);
}

//utils

function handleLink(on: string, to: string): E.Either<LinkError, LinkResult> {
	return F.pipe(
		linkName(on),
		E.chainW(doExists()),
		E.chainW(doExistingOrError(on)),
		E.map(({ original }) => ({ on, to, original }))
	);
}

function handleDirectory(on: string, to: string): E.Either<LinkError, LinkResult> {
	return F.pipe(
		linkName(on),
		E.chainW(doRenameToBackup(on)),
		E.chainW(doSymlinkOrRevert(to, "dir")),
		E.map(({ original }) => ({ on, to, original }))
	);
}

function handleFile(on: string, to: string): E.Either<LinkError, LinkResult> {
	return F.pipe(
		linkName(on),
		E.chainW(doRenameToBackup(on)),
		E.chainW(doSymlinkOrRevert(to, "file")),
		E.map(({ original }) => ({ on, to, original }))
	);
}

function handleUnlink(on: string): E.Either<UnlinkError, UnlinkResult> {
	return F.pipe(
		linkName(on),
		E.chainW(doExists()),
		E.chainW(doUnlinkOrError(on)),
		E.chainW(doRenameFromBackup(on)),
		E.map(() => ({ on }))
	);
}

function handleLinked(on: string): E.Either<never, LinkedResult> {
	return F.pipe(
		linkName(on),
		E.chainW(doExists()),
		E.map(({ exists, link }) => ({ on, exists, backup: link })),
		E.orElseW(() => handleNotLinked(on))
	);
}

function handleNotLinked(on: string): E.Either<never, LinkedResult> {
	return F.pipe(
		linkName(on),
		E.map((backup) => ({ on, exists: false, backup }))
	);
}

function handleNoSymlink(on: string): E.Either<UnlinkError, UnlinkResult> {
	return doNoSymlink(on);
}

function handleUnknown(on: string): E.Either<LinkError, LinkResult> {
	return doUnknown(on);
}
