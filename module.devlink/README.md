# ![Servant][logo] devlink-core

[![Pipeline][pipeline]][link-pipeline]
[![Npm][npm-version]][link-npm]
[![Downloads][npm-downloads]][link-npm]
[![License][license]][link-license]
[![Node][node]][link-node]
[![Collaborators][collaborators]][link-npm]

## What is it?

Devlink is a simple tool, that can create symbolic link in your system from source
to destination file. Usage is really simple and there are more ways how to use this
api. It's implemented as **functional**, **promise**, **throwable** and with **callbacks**.
Devlink keeps original file, so you can always revert link operation by unlink operation,
that's return previous file.

## **functional API**

### function `link(on: string, to: string): Either<LinkError, LinkResult>`

This is a functional alternative of `link` method. This method take 2 parameters and return
Either monad, that contains `LinkError` if method fail or `LinkResults` if everything is
done correctly. This method create link from **`on`** file to **`to`** file.

**Parameters:**

1. **`on`** - path to file when we need to create link, file must exist
2. **`to`** - path to file where link will be pointing, file must exist

**Returns:**

-   **`Either<LinkError, LinkResult>`**

**Example:**

```typescript
import { functional } from "devlink-core";

const { link } = functional;
const result = link("/data/sources/source.txt", "/data/links/link.txt");
```

### function `unlink(on: string): Either<UnlinkError, UnlinkResult>`

This is a functional alternative of `unlink` method. This method take 1 parameter and return
Either monad, that contains `UnlinkError` if method fail or `UnlinkResult` if everything is
done correctly. This method remove link created by `link` method on **`on`** file.

**Parameters:**

1. **`on`** - path to file where link is created, file must exist

**Returns:**

-   **`Either<UnlinkError, UnlinkResult>`**

**Example:**

```typescript
import { functional } from "devlink-core";

const { unlink } = functional;
const result = unlink("/data/sources/source.txt");
```

### function `linked(on: string): E.Either<never, LinkedResult>`

This is a functional alternative of `linked` method. This method take 1 parameter and return
Either monad, that contains `LinkedResult` if everything is done correctly. This method never fail.
This method is used to check if **`on`** file exists link, that is created by `link` function.

**Parameters:**

1. **`on`** - path to file where we want to check if link is created

**Returns:**

-   **`Either<never, LinkedResult>`**

**Example:**

```typescript
import { functional } from "devlink-core";

const { linked } = functional;
const result = linked("/data/sources/source.txt");
```

## **promise API**

### function `link(on: string, to: string): Promise<LinkResult>`

This is a promise alternative of `link` method. This method take 2 parameters and return
promise, that return `LinkResults` if everything is done correctly and catch `LinkError` if fails.
This method create link from **`on`** file to **`to`** file.

**Parameters:**

1. **`on`** - path to file when we need to create link, file must exist
2. **`to`** - path to file where link will be pointing, file must exist

**Returns:**

-   **`Promise<LinkResult>`**

**Catch:**

-   **`LinkError`**

**Example:**

```typescript
import { promise } from "devlink-core";

const { link } = promise;
link("/data/sources/source.txt", "/data/links/link.txt")
	.then((result) => {})
	.catch((err) => {});
```

### function `unlink(on: string): Promise<UnlinkResult>`

This is a promise alternative of `unlink` method. This method take 1 parameter and return
promise, that return `UnlinkResult` if everything is done correctly and catch `LinkError` if fails.
This method remove link created by `link` method on **`on`** file.

**Parameters:**

1. **`on`** - path to file where link is created, file must exist

**Returns:**

-   **`Promise<UnlinkResult>`**

**Catch:**

-   **`LinkError`**

**Example:**

```typescript
import { promise } from "devlink-core";

const { unlink } = promise;
unlink("/data/sources/source.txt")
	.then((result) => {})
	.catch((err) => {});
```

### function `linked(on: string): Promise<LinkedResult | null>`

This is a promise alternative of `linked` method. This method take 1 parameter and return
promise, that contains `LinkedResult` if everything is done correctly. This method never fail.
This method is used to check if **`on`** file exists link, that is created by `link` function.

**Parameters:**

1. **`on`** - path to file where we want to check if link is created

**Returns:**

-   **`Promise<LinkedResult | null>`**

**Example:**

```typescript
import { promise } from "devlink-core";

const { linked } = promise;
linked("/data/sources/source.txt").then((result) => {});
```

## **throwable API**

### function `link(on: string, to: string): LinkResult`

This is a throwable alternative of `link` method. This method take 2 parameters and return
result `LinkResults` if everything is done correctly and throw `LinkThrow` if fails.
This method create link from **`on`** file to **`to`** file.

**Parameters:**

1. **`on`** - path to file when we need to create link, file must exist
2. **`to`** - path to file where link will be pointing, file must exist

**Returns:**

-   **`LinkResult`**

**Throw:**

-   **`LinkThrow`**

**Example:**

```typescript
import { throwable } from "devlink-core";

const { link } = throwable;
try {
	const results = link("/data/sources/source.txt", "/data/links/link.txt");
} catch (err) {
	// handle error
}
```

### function `unlink(on: string): Promise<UnlinkResult>`

This is a throwable alternative of `unlink` method. This method take 1 parameter and
return `UnlinkResult` if everything is done correctly and throw `LinkThrow` if fails.
This method remove link created by `link` method on **`on`** file.

**Parameters:**

1. **`on`** - path to file where link is created, file must exist

**Returns:**

-   **`UnlinkResult`**

**Throw:**

-   **`LinkThrow`**

**Example:**

```typescript
import { throwable } from "devlink-core";

const { unlink } = throwable;
try {
	const results = unlink("/data/sources/source.txt");
} catch (err) {
	// handle error
}
```

### function `linked(on: string): Promise<LinkedResult | null>`

This is a throwable alternative of `linked` method. This method take 1 parameter and return
`LinkedResult` if everything is done correctly or `null` if there is some error. This method never fail.
This method is used to check if **`on`** file exists link, that is created by `link` function.

**Parameters:**

1. **`on`** - path to file where we want to check if link is created

**Returns:**

-   **`LinkedResult | null`**

**Example:**

```typescript
import { throwable } from "devlink-core";

const { linked } = throwable;
const result = linked("/data/sources/source.txt");
```

## **callback API**

### function `link(on: string, to: string, onDone: (err: LinkThrow | null, res: LinkResult | null) => void): void`

This is a callback alternative of `link` method. This method take 3 parameters and return void.
Last parameter is callback function with error `LinkThrow` and result `LinkResults`. If something failed
result parameter is `null`, is everything is ok, err parameter is `null`.
This method create link from **`on`** file to **`to`** file.

**Parameters:**

1. **`on`** - path to file when we need to create link, file must exist
2. **`to`** - path to file where link will be pointing, file must exist
3. **`onDone`** - callback function with err and result

**Example:**

```typescript
import { callback } from "devlink-core";

const { link } = callback;
link("/data/sources/source.txt", "/data/links/link.txt", (err, result) => {
	if (result) {
		// handle result
	}
	if (err) {
		// handle error
	}
});
```

### function `unlink(on: string, onDone: (err: LinkThrow | null, res: UnlinkResult | null) => void): void`

This is a callback alternative of `unlink` method. This method take 2 parameters and return void.
Last parameter is callback function with error `LinkThrow` and result `UnlinkResult`. If something failed
result parameter is `null`, is everything is ok, err parameter is `null`.
This method remove link created by `link` method on **`on`** file.

**Parameters:**

1. **`on`** - path to file where link is created, file must exist
2. **`onDone`** - callback function with err and result

**Example:**

```typescript
import { callback } from "devlink-core";

const { unlink } = callback;
unlink("/data/sources/source.txt", (err, result) => {
	if (result) {
		// handle result
	}
	if (err) {
		// handle error
	}
});
```

### function `linked(on: string): Promise<LinkedResult | null>`

This is a callback alternative of `linked` method. This method take 2 parameters and return void.
Last parameter is callback function with result `UnlinkResult` or `null`. If something failed
result parameter is `null`.
This method is used to check if **`on`** file exists link, that is created by `link` function.

**Parameters:**

1. **`on`** - path to file where we want to check if link is created
2. **`onDone`** - callback function with err and result

**Example:**

```typescript
import { callback } from "devlink-core";

const { linked } = callback;
linked("/data/sources/source.txt", (result) => {
	if (result) {
		// handle result
	} else {
		// handle error
	}
});
```

### Donate me

| QR                                                                                                           | Paypal                                                                                                                                                              |
| ------------------------------------------------------------------------------------------------------------ | ------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| ![](https://gitlab.com/uploads/-/system/personal_snippet/1929487/66399a49a06fa8eb9a0758b8673758c5/qr_sh.png) | [![](https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=DUT8W343NVGQQ&source=url) |

[logo]: https://gitlab.com/stanislavhacker/devlink/raw/master/logo.png
[preview]: https://gitlab.com/stanislavhacker/devlink/raw/master/assets/preview.jpg
[pipeline]: https://gitlab.com/stanislavhacker/devlink/badges/master/pipeline.svg
[npm-version]: https://img.shields.io/npm/v/devlink-core.svg
[npm-downloads]: https://img.shields.io/npm/dm/devlink-core.svg
[license]: https://img.shields.io/npm/l/devlink-core.svg
[node]: https://img.shields.io/node/v/devlink-core.svg
[collaborators]: https://img.shields.io/npm/collaborators/devlink-core.svg
[issues]: https://gitlab.com/stanislavhacker/devlink/-/issues/
[link-license]: https://gitlab.com/stanislavhacker/devlink/blob/master/LICENSE
[link-npm]: https://www.npmjs.com/package/devlink-cli
[link-pipeline]: https://gitlab.com/stanislavhacker/devlink/pipelines
[link-node]: https://nodejs.org/en/
[link-api]: https://gitlab.com/stanislavhacker/devlink/-/blob/master/module.devlink/README.md
[link-devlink-package]: https://www.npmjs.com/package/devlink-core
